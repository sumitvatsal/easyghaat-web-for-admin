﻿using System.Web;
using System.Web.Optimization;

namespace EasyGhaat
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            //here required JS & CSS for customerdashboard

            //CSS
            bundles.Add(new StyleBundle("~/css/customer").Include(
                   "~/Content/sweetalert-master/sweetalert.css"));

            //JS
            bundles.Add(new ScriptBundle("~/js/customer").Include(
                      "~/Content/js/jquery.js",
                      "~/Content/sweetalert-master/sweetalert.min.js",
                      "~/Areas/Customerdashboard/Content/js/Cart_function.js",
                      "~/Areas/Customerdashboard/Content/js/date.js"
                      ));

        }
    }
}
