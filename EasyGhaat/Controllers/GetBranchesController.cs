﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class GetBranchsController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/GetBranchs
        public IQueryable<GetBranch> GetGetBranchs()
        {
            return db.GetBranchs;
        }

        // GET: api/GetBranchs/5
        [ResponseType(typeof(GetBranch))]
        public async Task<IHttpActionResult> GetGetBranch(int id)
        {
            GetBranch getBranch = await db.GetBranchs.FindAsync(id);
            if (getBranch == null)
            {
                return NotFound();
            }

            return Ok(getBranch);
        }

        // PUT: api/GetBranchs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGetBranch(int id, GetBranch getBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != getBranch.BranchId)
            {
                return BadRequest();
            }

            db.Entry(getBranch).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GetBranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GetBranchs
        [ResponseType(typeof(GetBranch))]
        public async Task<IHttpActionResult> PostGetBranch(GetBranch getBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GetBranchs.Add(getBranch);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = getBranch.BranchId }, getBranch);
        }

        // DELETE: api/GetBranchs/5
        [ResponseType(typeof(GetBranch))]
        public async Task<IHttpActionResult> DeleteGetBranch(int id)
        {
            GetBranch getBranch = await db.GetBranchs.FindAsync(id);
            if (getBranch == null)
            {
                return NotFound();
            }

            db.GetBranchs.Remove(getBranch);
            await db.SaveChangesAsync();

            return Ok(getBranch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GetBranchExists(int id)
        {
            return db.GetBranchs.Count(e => e.BranchId == id) > 0;
        }
    }
}