﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class GetRolesController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/GetRoles
        public IQueryable<GetRole> GetGetRoles()
        {
            return db.GetRoles.Where(r => r.RoleName != "Customer" && r.RoleName != "Admin");
        }

        // GET: api/GetRoles/5
        [ResponseType(typeof(GetRole))]
        public async Task<IHttpActionResult> GetGetRole(int id)
        {
            GetRole getRole = await db.GetRoles.FindAsync(id);
            if (getRole == null)
            {
                return NotFound();
            }

            return Ok(getRole);
        }

        // PUT: api/GetRoles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGetRole(int id, GetRole getRole)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != getRole.RoleId)
            {
                return BadRequest();
            }

            db.Entry(getRole).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GetRoleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GetRoles
        [ResponseType(typeof(GetRole))]
        public async Task<IHttpActionResult> PostGetRole(GetRole getRole)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GetRoles.Add(getRole);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = getRole.RoleId }, getRole);
        }

        // DELETE: api/GetRoles/5
        [ResponseType(typeof(GetRole))]
        public async Task<IHttpActionResult> DeleteGetRole(int id)
        {
            GetRole getRole = await db.GetRoles.FindAsync(id);
            if (getRole == null)
            {
                return NotFound();
            }

            db.GetRoles.Remove(getRole);
            await db.SaveChangesAsync();

            return Ok(getRole);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GetRoleExists(int id)
        {
            return db.GetRoles.Count(e => e.RoleId == id) > 0;
        }
    }
}