﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class GetEmployeebyRoleAndBranchsController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/GetEmployeebyRoleAndBranchs
        [Route("api/GetEmployeebyRoleAndBranchs/{BranchId}/{RoleId}")]
        public IQueryable<GetEmployeebyRoleAndBranch> GetGetEmployeebyRoleAndBranchs(int BranchId, int RoleId)
        {
            return db.GetEmployeebyRoleAndBranchs.Where(e => e.BranchId == BranchId && e.RoleId == RoleId);
        }

        // GET: api/GetEmployeebyRoleAndBranchs/5
        [ResponseType(typeof(GetEmployeebyRoleAndBranch))]
        public async Task<IHttpActionResult> GetGetEmployeebyRoleAndBranch(int id)
        {
            GetEmployeebyRoleAndBranch getEmployeebyRoleAndBranch = await db.GetEmployeebyRoleAndBranchs.FindAsync(id);
            if (getEmployeebyRoleAndBranch == null)
            {
                return NotFound();
            }

            return Ok(getEmployeebyRoleAndBranch);
        }

        // PUT: api/GetEmployeebyRoleAndBranchs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGetEmployeebyRoleAndBranch(int id, GetEmployeebyRoleAndBranch getEmployeebyRoleAndBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != getEmployeebyRoleAndBranch.EmployeeId)
            {
                return BadRequest();
            }

            db.Entry(getEmployeebyRoleAndBranch).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GetEmployeebyRoleAndBranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GetEmployeebyRoleAndBranchs
        [ResponseType(typeof(GetEmployeebyRoleAndBranch))]
        public async Task<IHttpActionResult> PostGetEmployeebyRoleAndBranch(GetEmployeebyRoleAndBranch getEmployeebyRoleAndBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GetEmployeebyRoleAndBranchs.Add(getEmployeebyRoleAndBranch);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = getEmployeebyRoleAndBranch.EmployeeId }, getEmployeebyRoleAndBranch);
        }

        // DELETE: api/GetEmployeebyRoleAndBranchs/5
        [ResponseType(typeof(GetEmployeebyRoleAndBranch))]
        public async Task<IHttpActionResult> DeleteGetEmployeebyRoleAndBranch(int id)
        {
            GetEmployeebyRoleAndBranch getEmployeebyRoleAndBranch = await db.GetEmployeebyRoleAndBranchs.FindAsync(id);
            if (getEmployeebyRoleAndBranch == null)
            {
                return NotFound();
            }

            db.GetEmployeebyRoleAndBranchs.Remove(getEmployeebyRoleAndBranch);
            await db.SaveChangesAsync();

            return Ok(getEmployeebyRoleAndBranch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GetEmployeebyRoleAndBranchExists(int id)
        {
            return db.GetEmployeebyRoleAndBranchs.Count(e => e.EmployeeId == id) > 0;
        }
    }
}