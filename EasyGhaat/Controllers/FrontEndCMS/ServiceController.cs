﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;


namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class ServiceController : Controller
    {
        AdminContext db = new AdminContext();


        // GET: Service
        public ActionResult Index()
        {
            // IList<service_page_stripe> obj = db.service_page_stripe.ToList();

            ViewModel mymodel = new ViewModel();
            mymodel.Aditional_servicepages = db.Aditional_servicepage.ToList();
            mymodel.service_page_stripes = db.service_page_stripe.ToList();
            mymodel.wlcm_srvic_stripes = db.wlcm_srvic_stripe.ToList();
            mymodel.Footer_content = db.Footer_content.ToList();
            return View(mymodel);
        }
    }
}