﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Net.Mail;

namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class TermAndConditionController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: TermAndCondition
        public ActionResult Index()
        {
            using (AdminContext db = new AdminContext())
            {
                ViewModel mymodel = new ViewModel();
                mymodel.sliders = db.home_slider.ToList();
                mymodel.welcome = db.Welocme_area.ToList();
                mymodel.testimonials = db.testimonials.ToList();
                mymodel.about_stripes = db.about_stripe.ToList();
                mymodel.Footer_content = db.Footer_content.ToList();
                mymodel.UpperFooters = db.UpperFooters.ToList();
                return View(mymodel);
            }
        }
    }
}