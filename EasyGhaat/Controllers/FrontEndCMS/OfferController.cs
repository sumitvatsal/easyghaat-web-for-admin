﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;

namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class OfferController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: Offer
        public ActionResult Index()
        {
            ViewModel mymodel = new ViewModel();
            mymodel.Offer = db.Offers.ToList();

            
             mymodel.Footer_content = db.Footer_content.ToList();
            return View(mymodel);
        }
    }
}