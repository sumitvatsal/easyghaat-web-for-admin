﻿using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;


namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class AditionalServiceController : Controller
    {

        AdminContext db = new AdminContext();

        // GET: AditionalService
        public ActionResult Index()
        {

            IList<Aditional_servicepage> obj = db.Aditional_servicepage.ToList();
            return View(obj);
        }
    }
}