﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Net.Mail;

namespace EasyGhaat.Controllers
{
    public class HomeController : Controller
    {

     AdminContext db = new AdminContext();

        public string EmailId = "";

        // GET: Home
        public ActionResult Index()
        {
            using (AdminContext db = new AdminContext())
            {
                ViewModel mymodel = new ViewModel();
                mymodel.sliders = db.home_slider.ToList();
                mymodel.welcome = db.Welocme_area.ToList();
                mymodel.testimonials = db.testimonials.ToList();
                mymodel.about_stripes = db.about_stripe.ToList();
                mymodel.Footer_content = db.Footer_content.ToList();
                mymodel.UpperFooters = db.UpperFooters.ToList();
                return View(mymodel);
            }
        }

        [HttpPost]
        public JsonResult Submit(string Name, string Email  )
        {
                      
                Mail_collection obj = new Mail_collection();
                EmailId = Email;
                obj.name = Name;
                obj.email = Email;
                db.Mail_collection.Add(obj);
                db.SaveChanges();
               // Mail();
                return Json(true, JsonRequestBehavior.AllowGet);             
        }


        


    }
}