﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace EasyGhaat.Controllers.FrontEndCMS
{
    public class AboutUsPageController : Controller
    {
        // GET: AboutUsPage
        public ActionResult Index()
        {
            using (AdminContext db = new AdminContext())
            { 
             
                ViewModel mymodel = new ViewModel();              
                mymodel.about_stripes = db.about_stripe.ToList();
                mymodel.Footer_content = db.Footer_content.ToList();
                return View(mymodel);
            }
        }
    }
}