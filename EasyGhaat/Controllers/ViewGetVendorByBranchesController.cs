﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetVendorByBranchesController : ApiController
    {
        private AdminContext db = new AdminContext();

  
        [Route("api/ViewGetVendorByBranches/{BranchId}")]
        public IQueryable<ViewGetVendorByBranch> GetViewGetVendorByBranchs( int BranchId)
        {
            return db.ViewGetVendorByBranchs.Where(v => v.AssignBranchId == BranchId);
        }

        // GET: api/ViewGetVendorByBranches/5
        [ResponseType(typeof(ViewGetVendorByBranch))]
        public async Task<IHttpActionResult> GetViewGetVendorByBranch(int id)
        {
            ViewGetVendorByBranch viewGetVendorByBranch = await db.ViewGetVendorByBranchs.FindAsync(id);
            if (viewGetVendorByBranch == null)
            {
                return NotFound();
            }

            return Ok(viewGetVendorByBranch);
        }

        // PUT: api/ViewGetVendorByBranches/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetVendorByBranch(int id, ViewGetVendorByBranch viewGetVendorByBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetVendorByBranch.Id)
            {
                return BadRequest();
            }

            db.Entry(viewGetVendorByBranch).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetVendorByBranchExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetVendorByBranches
        [ResponseType(typeof(ViewGetVendorByBranch))]
        public async Task<IHttpActionResult> PostViewGetVendorByBranch(ViewGetVendorByBranch viewGetVendorByBranch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetVendorByBranchs.Add(viewGetVendorByBranch);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetVendorByBranch.Id }, viewGetVendorByBranch);
        }

        // DELETE: api/ViewGetVendorByBranches/5
        [ResponseType(typeof(ViewGetVendorByBranch))]
        public async Task<IHttpActionResult> DeleteViewGetVendorByBranch(int id)
        {
            ViewGetVendorByBranch viewGetVendorByBranch = await db.ViewGetVendorByBranchs.FindAsync(id);
            if (viewGetVendorByBranch == null)
            {
                return NotFound();
            }

            db.ViewGetVendorByBranchs.Remove(viewGetVendorByBranch);
            await db.SaveChangesAsync();

            return Ok(viewGetVendorByBranch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetVendorByBranchExists(int id)
        {
            return db.ViewGetVendorByBranchs.Count(e => e.Id == id) > 0;
        }
    }
}