﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetAssignedServiceByVendorIdsController : ApiController
    {
        private AdminContext db = new AdminContext();
        
        [Route("api/ViewGetAssignedServiceByVendorIds/{VendorId}")]
        public IQueryable<ViewGetAssignedServiceByVendorId> GetViewGetAssignedServiceByVendorIds(int VendorId)
        {
            return db.ViewGetAssignedServiceByVendorIds.Where(v => v.VendorId == VendorId);
        }

        // GET: api/ViewGetAssignedServiceByVendorIds/5
        [ResponseType(typeof(ViewGetAssignedServiceByVendorId))]
        public async Task<IHttpActionResult> GetViewGetAssignedServiceByVendorId(int id)
        {
            ViewGetAssignedServiceByVendorId viewGetAssignedServiceByVendorId = await db.ViewGetAssignedServiceByVendorIds.FindAsync(id);
            if (viewGetAssignedServiceByVendorId == null)
            {
                return NotFound();
            }

            return Ok(viewGetAssignedServiceByVendorId);
        }

        // PUT: api/ViewGetAssignedServiceByVendorIds/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetAssignedServiceByVendorId(int id, ViewGetAssignedServiceByVendorId viewGetAssignedServiceByVendorId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetAssignedServiceByVendorId.VendorId)
            {
                return BadRequest();
            }

            db.Entry(viewGetAssignedServiceByVendorId).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetAssignedServiceByVendorIdExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetAssignedServiceByVendorIds
        [ResponseType(typeof(ViewGetAssignedServiceByVendorId))]
        public async Task<IHttpActionResult> PostViewGetAssignedServiceByVendorId(ViewGetAssignedServiceByVendorId viewGetAssignedServiceByVendorId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetAssignedServiceByVendorIds.Add(viewGetAssignedServiceByVendorId);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetAssignedServiceByVendorId.VendorId }, viewGetAssignedServiceByVendorId);
        }

        // DELETE: api/ViewGetAssignedServiceByVendorIds/5
        [ResponseType(typeof(ViewGetAssignedServiceByVendorId))]
        public async Task<IHttpActionResult> DeleteViewGetAssignedServiceByVendorId(int id)
        {
            ViewGetAssignedServiceByVendorId viewGetAssignedServiceByVendorId = await db.ViewGetAssignedServiceByVendorIds.FindAsync(id);
            if (viewGetAssignedServiceByVendorId == null)
            {
                return NotFound();
            }

            db.ViewGetAssignedServiceByVendorIds.Remove(viewGetAssignedServiceByVendorId);
            await db.SaveChangesAsync();

            return Ok(viewGetAssignedServiceByVendorId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetAssignedServiceByVendorIdExists(int id)
        {
            return db.ViewGetAssignedServiceByVendorIds.Count(e => e.VendorId == id) > 0;
        }
    }
}