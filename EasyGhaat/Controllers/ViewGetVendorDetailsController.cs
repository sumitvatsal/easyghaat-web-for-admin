﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetVendorDetailsController : ApiController
    {
        private AdminContext db = new AdminContext();

        [Route("api/ViewGetVendorDetails/{VendorId}")]
        public IQueryable<ViewGetVendorDetail> GetViewGetVendorDetails(int VendorId)
        {
            return db.ViewGetVendorDetails.Where(v => v.Id == VendorId);
        }

        // GET: api/ViewGetVendorDetails/5
        [ResponseType(typeof(ViewGetVendorDetail))]
        public async Task<IHttpActionResult> GetViewGetVendorDetail(int id)
        {
            ViewGetVendorDetail viewGetVendorDetail = await db.ViewGetVendorDetails.FindAsync(id);
            if (viewGetVendorDetail == null)
            {
                return NotFound();
            }

            return Ok(viewGetVendorDetail);
        }

        // PUT: api/ViewGetVendorDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetVendorDetail(int id, ViewGetVendorDetail viewGetVendorDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetVendorDetail.Id)
            {
                return BadRequest();
            }

            db.Entry(viewGetVendorDetail).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetVendorDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetVendorDetails
        [ResponseType(typeof(ViewGetVendorDetail))]
        public async Task<IHttpActionResult> PostViewGetVendorDetail(ViewGetVendorDetail viewGetVendorDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetVendorDetails.Add(viewGetVendorDetail);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetVendorDetail.Id }, viewGetVendorDetail);
        }

        // DELETE: api/ViewGetVendorDetails/5
        [ResponseType(typeof(ViewGetVendorDetail))]
        public async Task<IHttpActionResult> DeleteViewGetVendorDetail(int id)
        {
            ViewGetVendorDetail viewGetVendorDetail = await db.ViewGetVendorDetails.FindAsync(id);
            if (viewGetVendorDetail == null)
            {
                return NotFound();
            }

            db.ViewGetVendorDetails.Remove(viewGetVendorDetail);
            await db.SaveChangesAsync();

            return Ok(viewGetVendorDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetVendorDetailExists(int id)
        {
            return db.ViewGetVendorDetails.Count(e => e.Id == id) > 0;
        }
    }
}