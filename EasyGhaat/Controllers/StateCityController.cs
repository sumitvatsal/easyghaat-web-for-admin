﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class StateCityController : ApiController
    {
        private AdminContext db = new AdminContext();

        [HttpGet]
        [ActionName("GetState")]
        [Route("api/StateCity/GetState")]
        public IQueryable<State> GetState()
        {
            return db.States;
        }


        [HttpGet]
        [ActionName("GetCities")]
        [Route("api/StateCity/GetCities")]
        public IQueryable<City> GetCities()
        {
            return db.Cities;
        }



        [HttpGet]
        [Route("api/StateCityController/GetCity_ByStateId/{StateId}")]
        public IQueryable<City> GetCity_ByStateId(int StateId)
        {
            return db.Cities.Where(cty=>cty.StateId==StateId);
        }
    }
}
