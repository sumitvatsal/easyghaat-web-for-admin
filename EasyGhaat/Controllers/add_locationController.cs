﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class add_locationController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/add_location
        public IQueryable<add_location> Getadd_location()
        {
            return db.add_location;
        }

        // GET: api/add_location/5
        [ResponseType(typeof(add_location))]
        public IHttpActionResult Getadd_location(int id)
        {
            add_location add_location = db.add_location.Find(id);
            if (add_location == null)
            {
                return NotFound();
            }

            return Ok(add_location);
        }

        // PUT: api/add_location/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putadd_location(int id, add_location add_location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != add_location.id)
            {
                return BadRequest();
            }

            db.Entry(add_location).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!add_locationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/add_location
        [ResponseType(typeof(add_location))]
        public IHttpActionResult Postadd_location(add_location add_location)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.add_location.Add(add_location);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = add_location.id }, add_location);
        }

        // DELETE: api/add_location/5
        [ResponseType(typeof(add_location))]
        public IHttpActionResult Deleteadd_location(int id)
        {
            add_location add_location = db.add_location.Find(id);
            if (add_location == null)
            {
                return NotFound();
            }

            db.add_location.Remove(add_location);
            db.SaveChanges();

            return Ok(add_location);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool add_locationExists(int id)
        {
            return db.add_location.Count(e => e.id == id) > 0;
        }
    }
}