﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class GetEmployeeAddressesController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/GetEmployeeAddresses
        [Route("api/GetEmployeeAddresses/{EmployeeId}")]
        public IQueryable<GetEmployeeAddress> GetAddress(int EmployeeId)
        {
            return db.GetEmployeeAddresses.Where(emp => emp.EmployeeId == EmployeeId && emp.Isdeleted==false);
        }

        // GET: api/GetEmployeeAddresses/5
        [ResponseType(typeof(GetEmployeeAddress))]
        public async Task<IHttpActionResult> GetGetEmployeeAddress(int id)
        {
            GetEmployeeAddress getEmployeeAddress = await db.GetEmployeeAddresses.FindAsync(id);
            if (getEmployeeAddress == null)
            {
                return NotFound();
            }

            return Ok(getEmployeeAddress);
        }

        // PUT: api/GetEmployeeAddresses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGetEmployeeAddress(int id, GetEmployeeAddress getEmployeeAddress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != getEmployeeAddress.EmployeeId)
            {
                return BadRequest();
            }

            db.Entry(getEmployeeAddress).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GetEmployeeAddressExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GetEmployeeAddresses
        [ResponseType(typeof(GetEmployeeAddress))]
        public async Task<IHttpActionResult> PostGetEmployeeAddress(GetEmployeeAddress getEmployeeAddress)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GetEmployeeAddresses.Add(getEmployeeAddress);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = getEmployeeAddress.EmployeeId }, getEmployeeAddress);
        }

        // DELETE: api/GetEmployeeAddresses/5
        [ResponseType(typeof(GetEmployeeAddress))]
        public async Task<IHttpActionResult> DeleteGetEmployeeAddress(int id)
        {
            GetEmployeeAddress getEmployeeAddress = await db.GetEmployeeAddresses.FindAsync(id);
            if (getEmployeeAddress == null)
            {
                return NotFound();
            }

            db.GetEmployeeAddresses.Remove(getEmployeeAddress);
            await db.SaveChangesAsync();

            return Ok(getEmployeeAddress);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GetEmployeeAddressExists(int id)
        {
            return db.GetEmployeeAddresses.Count(e => e.EmployeeId == id) > 0;
        }
    }
}