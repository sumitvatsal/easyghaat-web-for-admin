﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class GetEmployeeDetailsController : ApiController
    {
        private AdminContext db = new AdminContext();

        // GET: api/GetEmployeeDetails

        [Route("api/GetEmployeeDetails/{EmployeeId}")]
        public IQueryable<GetEmployeeDetail> GetGetEmployeeDetails(int EmployeeId)
        {
            return db.GetEmployeeDetails.Where(emp => emp.EmployeeId == EmployeeId);
        }

        // GET: api/GetEmployeeDetails/5
        [ResponseType(typeof(GetEmployeeDetail))]
        public async Task<IHttpActionResult> GetGetEmployeeDetail(int id)
        {
            GetEmployeeDetail getEmployeeDetail = await db.GetEmployeeDetails.FindAsync(id);
            if (getEmployeeDetail == null)
            {
                return NotFound();
            }

            return Ok(getEmployeeDetail);
        }

        // PUT: api/GetEmployeeDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutGetEmployeeDetail(int id, GetEmployeeDetail getEmployeeDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != getEmployeeDetail.EmployeeId)
            {
                return BadRequest();
            }

            db.Entry(getEmployeeDetail).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GetEmployeeDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/GetEmployeeDetails
        [ResponseType(typeof(GetEmployeeDetail))]
        public async Task<IHttpActionResult> PostGetEmployeeDetail(GetEmployeeDetail getEmployeeDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.GetEmployeeDetails.Add(getEmployeeDetail);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = getEmployeeDetail.EmployeeId }, getEmployeeDetail);
        }

        // DELETE: api/GetEmployeeDetails/5
        [ResponseType(typeof(GetEmployeeDetail))]
        public async Task<IHttpActionResult> DeleteGetEmployeeDetail(int id)
        {
            GetEmployeeDetail getEmployeeDetail = await db.GetEmployeeDetails.FindAsync(id);
            if (getEmployeeDetail == null)
            {
                return NotFound();
            }

            db.GetEmployeeDetails.Remove(getEmployeeDetail);
            await db.SaveChangesAsync();

            return Ok(getEmployeeDetail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GetEmployeeDetailExists(int id)
        {
            return db.GetEmployeeDetails.Count(e => e.EmployeeId == id) > 0;
        }
    }
}