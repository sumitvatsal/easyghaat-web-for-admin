﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using EasyGhaat.Models;

namespace EasyGhaat.Controllers
{
    public class ViewGetVendorByBranchIdsController : ApiController
    {
        private AdminContext db = new AdminContext();

        [Route("api/ViewGetVendorByBranchIds/{BranchId}")]
        public IQueryable<ViewGetVendorByBranchId> GetViewGetVendorByBranchIds(int BranchId)
        {
            return db.ViewGetVendorByBranchIds.Where(ab => ab.BranchId == BranchId && ab.IsDeleted==false && ab.IsActive==true );
        }

        // GET: api/ViewGetVendorByBranchIds/5
        [ResponseType(typeof(ViewGetVendorByBranchId))]
        public async Task<IHttpActionResult> GetViewGetVendorByBranchId(int id)
        {
            ViewGetVendorByBranchId viewGetVendorByBranchId = await db.ViewGetVendorByBranchIds.FindAsync(id);
            if (viewGetVendorByBranchId == null)
            {
                return NotFound();
            }

            return Ok(viewGetVendorByBranchId);
        }

        // PUT: api/ViewGetVendorByBranchIds/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutViewGetVendorByBranchId(int id, ViewGetVendorByBranchId viewGetVendorByBranchId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != viewGetVendorByBranchId.Id)
            {
                return BadRequest();
            }

            db.Entry(viewGetVendorByBranchId).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViewGetVendorByBranchIdExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ViewGetVendorByBranchIds
        [ResponseType(typeof(ViewGetVendorByBranchId))]
        public async Task<IHttpActionResult> PostViewGetVendorByBranchId(ViewGetVendorByBranchId viewGetVendorByBranchId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ViewGetVendorByBranchIds.Add(viewGetVendorByBranchId);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = viewGetVendorByBranchId.Id }, viewGetVendorByBranchId);
        }

        // DELETE: api/ViewGetVendorByBranchIds/5
        [ResponseType(typeof(ViewGetVendorByBranchId))]
        public async Task<IHttpActionResult> DeleteViewGetVendorByBranchId(int id)
        {
            ViewGetVendorByBranchId viewGetVendorByBranchId = await db.ViewGetVendorByBranchIds.FindAsync(id);
            if (viewGetVendorByBranchId == null)
            {
                return NotFound();
            }

            db.ViewGetVendorByBranchIds.Remove(viewGetVendorByBranchId);
            await db.SaveChangesAsync();

            return Ok(viewGetVendorByBranchId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ViewGetVendorByBranchIdExists(int id)
        {
            return db.ViewGetVendorByBranchIds.Count(e => e.Id == id) > 0;
        }
    }
}