﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;

namespace EasyGhaat.Models
{




    public class Connection
    {
        public static string connstring = ConfigurationManager.ConnectionStrings["AdminContext"].ConnectionString;
    }


    [Table("City")]
    public class City
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "CityName cannot be blank")]
        public string CityName { get; set; }
        public bool? IsActive { get; set; }
        [Required(ErrorMessage = "StateId cannot be blank")]
        public int StateId { get; set; }
    }



    [Table("Emp_Info")]
    public class Emp_Info
    {
        [Key]
        public int emp_id { get; set; }
        public int RoleId { get; set; }
        public string emp_name { get; set; }
        public string password { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<System.DateTime> DOJ { get; set; }
        public string EmailId { get; set; }
        public string Name { get; set; }
        public string per_phoneNo { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public string RoleName { get; set; }
    }



    [Table("State")]
    public class State
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "StateName cannot be blank")]
        public string StateName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
    }




    [Table("CustomerFeedback")]
    public class CustomerFeedback
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
    }



    [Table("tblImage")]
    public class tblImage
    {
        [Key]
        public int id { get; set; }
        public string Photo { get; set; }
        public string PhotoName { get; set; }
    }



    [Table("GetBranch")]
    public class GetBranch
    {
        [Key]
        public int BranchId { get; set; }
        public string BranchName { get; set; }
    }



    [Table("GetRole")]
    public class GetRole
    {
        [Key]
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }


    [Table("GetEmployeebyRoleAndBranch")]
    public class GetEmployeebyRoleAndBranch
    {
        [Key]
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int RoleId { get; set; }
        public int BranchId { get; set; }
    }

    [Table("GetEmployeeDetail")]
    public class GetEmployeeDetail
    {
        [Key]
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PhoenNo { get; set; }
        public string EmailId { get; set; }
        public string DOB { get; set; }
        public string DOJ { get; set; }
        public bool Isdeleted { get; set; }
        public Nullable<double> BasicSalary { get; set; }
        public Nullable<double> TA { get; set; }
        public Nullable<double> DA { get; set; }
        public Nullable<double> HRA { get; set; }
        public string BankAccountNumber { get; set; }
        public string IFSC { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string MICR { get; set; }
        public string AccountType { get; set; }
    }


    [Table("ViewGetVendorDetail")]
    public class ViewGetVendorDetail
    {
        [Key]
        public int Id { get; set; }
        public string VendorName { get; set; }
        public string Password { get; set; }
        public string PhoneNo { get; set; }
        public string Per_PhoneNo { get; set; }
        public Nullable<DateTime> DOJ { get; set; }
        public Nullable<DateTime> DateOfContract { get; set; }
        public string ServiceTaxNo { get; set; }
        public string Email { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSC { get; set; }
        public string MICR { get; set; }
        public string AccountType { get; set; }
    }

    [Table("ViewGetVendorByBranchId")]
    public class ViewGetVendorByBranchId
    {
        [Key]
        public int Id { get; set; }
        public string VendorName { get; set; }
        public int BranchId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }

    [Table("ViewGetCancelReason")]
    public class ViewGetCancelReason
    {
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> RoleId { get; set; }
    }


    [Table("GetEmployeeAddress")]
    public class GetEmployeeAddress
    {
        [Key]
        public int EmployeeId { get; set; }
        public bool Isdeleted { get; set; }
        public string AddressType { get; set; }
        public string CityName { get; set; }
        public string AreaId { get; set; }
        public string HouseNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
    }

    [Table("ViewGetVendorAddress")]
    public class ViewGetVendorAddress
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public string AreaId { get; set; }
        public string House_ShopNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public string CityName { get; set; }
        public string AddressType { get; set; }
        public bool IsDeleted { get; set; }

    }

    [Table("ViewGetAssignedServiceByVendorId")]
    public class ViewGetAssignedServiceByVendorId
    {
        [Key]
        public int VendorId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
    }


    [Table("ViewGetVendorByBranch")]
    public class ViewGetVendorByBranch
    {
        [Key]
        public int Id { get; set; }
        public string VendorName { get; set; }
        public int AssignBranchId { get; set; }
    }

    [Table("PromoCode")]
    public class PromoCode
    {
        [Key]
        public int Id { get; set; }
        public string Promo_Code { get; set; }
        public int MaxUsers { get; set; }
        public System.DateTime ExpiryDate { get; set; }
        public double MinOrder { get; set; }
        public int PromoType { get; set; }
        public Nullable<int> Percentage { get; set; }
        public Nullable<int> MaxDiscount { get; set; }
        public int UsedCount { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<double> FlatAmount { get; set; }
        public int AppliedId { get; set; }

        [ForeignKey("PromoType")]
        public Promo_Type Promo_Type { get; set; }
    }

    [Table("PromoCode_Service")]
    public class PromoCode_Service
    {
        [Key]
        public int Id { get; set; }
        public int PromoCode_Id { get; set; }
        public int ServiceId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }

    [Table("Applying_pro")]
    public class Applying_pro
    {
        [Key]
        public int Id { get; set; }
        public string Apply_type { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

    }
    [Table("EmployeeSession")]
    public class EmployeeSession
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string LoginFrom { get; set; }
        public Nullable<System.DateTime> Login_DateTime { get; set; }
        public Nullable<System.DateTime> LogOut_DateTime { get; set; }
    }


    [Table("Product")]
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> GenderId { get; set; }
        [ForeignKey("CategoryId")]
        public Category Category { get; set; }
        [ForeignKey("GenderId")]
        public Gender Gender { get; set; }
    }


    [Table("Category")]
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }

    [Table("Gender")]
    public class Gender
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Imgurl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public virtual ICollection<Product> Product { get; set; }

    }

    [Table("about_stripe")]
    public class about_stripe
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string sub_title { get; set; }
        public string about_content { get; set; }
        public string ReadMore_Content { get; set; }
        public string RightSideContent { get; set; }
    }


    [Table("ActivityNote")]
    public class ActivityNote
    {
        [Key]
        public long ActivityNoteId { get; set; }
        public long ActivityId { get; set; }
        public Nullable<long> UserId { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> NoteDate { get; set; }
        public string Status { get; set; }
    }



    [Table("add_location")]
    public class add_location
    {
        [Key]
        public int id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string lat { get; set; }
        public string Long { get; set; }
    }




    [Table("Add_logo")]
    public partial class Add_logo
    {
        [Key]
        public int id { get; set; }
        public string Logo_image { get; set; }
        public string logo_path { get; set; }

    }

    [Table("Add_SignUp")]
    public  class Add_SignUp
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string BackGroundImage { get; set; }
    }




    [Table("Address")]
    public partial class Address
    {
        [Key]
        public int Id { get; set; }
        public int AddTypeId { get; set; }
        public int CityId { get; set; }
        public string AreaId { get; set; }
        public string HosueNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int CustomerId { get; set; }

        public virtual City City { get; set; }  
        public virtual Address_type Address_type { get; set; }
    }


    [Table("Address_type")]
    public partial class Address_type
    {
      
        [Key]
        public int Id { get; set; }
        public string Add_Type { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }

    [Table("Aditional_servicepage")]
    public class Aditional_servicepage
    {
        [Key]
        public int id { get; set; }
        public string box_icon { get; set; }
        public string box_heading { get; set; }
        public string box_content { get; set; }
    }

    [Table("AssignService_Vendor")]
    public   class AssignService_Vendor
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int ServiceId { get; set; }
        public bool IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public virtual Service Service { get; set; }
        public virtual Vendor Vendor { get; set; }
    }

    [Table("Attendance")]
    public  class Attendance
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> EmpId { get; set; }
        public Nullable<System.DateTime> LoginDate { get; set; }
        public Nullable<System.DateTime> LogOutDate { get; set; }
        public string LoginTime { get; set; }
        public string LogOutTime { get; set; }
        public string Attendance1 { get; set; }
        public string a_Count { get; set; }
        public string Hour { get; set; }
        public string MacId { get; set; }
        public string TotalKm { get; set; }
        public string el_Count { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string Status { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public virtual Employee Employee { get; set; }
    }


    [Table("BankAccount_Tbl")]
    public partial class BankAccount_Tbl
    {
       
        [Key]
        public int Id { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSC { get; set; }
        public string MICR { get; set; }
        public string AccountType { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
    }

    [Table("BankCashSubmit_Tbl")]
    public  class BankCashSubmit_Tbl
    {
        [Key]
        public int Id { get; set; }
        public int ManagerEmpId { get; set; }
        public int BankAccountID { get; set; }
        public double TotalAmount { get; set; }
        public string BankRefNo { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public int ConfirmedStatusId { get; set; }
        public Nullable<int> ReviewerEmpID { get; set; }
        public string RejectionReason { get; set; }
        public Nullable<System.DateTime> ReviewDateTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }

        public virtual BankAccount_Tbl BankAccount_Tbl { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Employee Employee1 { get; set; }
    }


    [Table("BankTransfer_Tbl")]
    public  class BankTransfer_Tbl
    {
       [Key]
        public int Id { get; set; }
        public int BeneficiaryId { get; set; }
        public int BeneficiaryTypeId { get; set; }
        public int TransferReasonId { get; set; }
        public double TransferedAmount { get; set; }
        public int FromBankAccountId { get; set; }
        public int BeneficiaryBankAccountId { get; set; }
        public string BankReferenceNo { get; set; }
        public string Remark { get; set; }
        public System.DateTime TransferDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }

        public virtual BankAccount_Tbl BankAccount_Tbl { get; set; }
        public virtual BeneficiaryType_Tbl BeneficiaryType_Tbl { get; set; }
        public virtual BankTransferReason_Tbl BankTransferReason_Tbl { get; set; }

    }


    [Table("BankTransferReason_Tbl")]
    public class BankTransferReason_Tbl
    {
    
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }

    }

    [Table("BeneficiaryType_Tbl")]
    public  class BeneficiaryType_Tbl
    {
       [Key]
        public int Id { get; set; }
        public string BeneficiaryType { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
    }

    [Table("Branch")]
    public  class Branch
    {
         [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNo { get; set; }
        public string EmailId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

    }

    [Table("BranchOffer")]
    public partial class BranchOffer
    {
        [Key]
        public int Id { get; set; }
        public int OfferTypeId { get; set; }
        public string OfferImage { get; set; }
        public Nullable<int> OfferCategory { get; set; }
        public string Offer_Serv { get; set; }
        public string Count_N { get; set; }
        public string bag { get; set; }
        public string Price { get; set; }
        public string DiscountPercentage { get; set; }
        public Nullable<System.DateTime> ValidFrom { get; set; }
        public Nullable<System.DateTime> ValidTill { get; set; }
        public string VaildDays { get; set; }
        public string OfferDetails { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string Discount { get; set; }
        public string MinAmount { get; set; }
        public Nullable<int> BranchId { get; set; }

        public virtual Branch Branch { get; set; }
    }




    [Table("CancelReason")]
    public class CancelReason
    {

        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual RoleType RoleType { get; set; }
    }






    [Table("CancelReasonByRoleType")]
    public  class CancelReasonByRoleType
    {
        [Key]
        public int Id { get; set; }
        public int ReasonId { get; set; }
        public string RoleName { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }


    [Table("Carpet_caltbl")]
    public  class Carpet_caltbl
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
    }



    [Table("Cashback_tbl")]
    public  class Cashback_tbl
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> OrderId { get; set; }
        public string Cashback_type { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> RcrgId { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> Trans_Date { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order_tbl Order_tbl { get; set; }

    }




    [Table("contact_content")]
    public  class contact_content
    {    [Key]
        public int id { get; set; }
        public string address { get; set; }
        public string contact_no { get; set; }
        public string email { get; set; }
    }


    [Table("DeliveryHours_tbl")]
    public partial class DeliveryHours_tbl
    {
        [Key]
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public int DeliveryTypeId { get; set; }
        public int BranchId { get; set; }
        public Nullable<int> DeliveryHours { get; set; }
        public Nullable<int> MarginalHours { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        [ForeignKey("DeliveryTypeId")]
        public virtual DeliveryType_tbl DeliveryType_tbl { get; set; }
        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
    }

    [Table("DeliveryHourstbl")]
    public class DeliveryHourstbl
    {
        [Key]
        public int DeliveryTypeId { get; set; }
        public string DeliveryType { get; set; }
        public int BranchId { get; set; }
    }



    [Table("DeliveryType_SurgePrice")]
    public  class DeliveryType_SurgePrice
    {
        [Key]
        public int Id { get; set; }
        public int DeliveryTypeId { get; set; }
        public int BranchId { get; set; }
        public int SurgePercentage { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        [ForeignKey("DeliveryTypeId")]
        public virtual DeliveryType_tbl DeliveryType_tbl { get; set; }



    }


    [Table("DeliveryType_tbl")]
    public  class DeliveryType_tbl
    {   
        [Key]
        public int Id { get; set; }
        public string DeliveryType { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }


    [Table("home_slider")]
    public class home_slider
    {
        [Key]
        public int slider_id { get; set; }
        public string image_path { get; set; }
        public string content_1 { get; set; }
        public string content_2 { get; set; }
    }






    [Table("EmpBankDetails")]
    public  class EmpBankDetails
    {
        [Key]
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public string BankAddress { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CraeteDate { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        [ForeignKey("EmpId")]
        public virtual Employee Employee { get; set; }
        [ForeignKey("StateId")]
        public virtual State State { get; set; }
    }




    [Table("EmpCashReceiving_Tbl")]
    public  class EmpCashReceiving_Tbl
    {
        [Key]
        public int Id { get; set; }
        public int TaskId { get; set; }
        public double TotalAmount { get; set; }
        public bool IsHandOver { get; set; }
        public Nullable<int> HandoverId { get; set; }
        public Nullable<System.DateTime> HandOverDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        [ForeignKey("TaskId")]
        public virtual TaskAssignment_tbl TaskAssignment_tbl { get; set; }
    }






    [Table("Employee")]
    public class Employee
    {

        [Key]
        public int emp_id { get; set; }
        public string emp_name { get; set; }
        public string password { get; set; }
        public int assin_roleId { get; set; }
        public Nullable<int> asin_branchId { get; set; }
        public string uplod_photo { get; set; }
        public bool IsActive { get; set; }
        public bool Isdeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string per_phoneNo { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public System.DateTime DOJ { get; set; }
        public string EmailId { get; set; }
        public int SecretPin { get; set; }

        [ForeignKey("assin_roleId")]
        public virtual RoleType RoleType { get; set; }
        [ForeignKey("asin_branchId")]
        public virtual Branch Branch { get; set; }

    }



    [Table("EmployeeAddress")]
    public  class EmployeeAddress
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int AddTypeId { get; set; }
        public int CityId { get; set; }
        public string AreaId { get; set; }
        public string HouseNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [ForeignKey("AddTypeId")]
        public virtual Address_type Address_type { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
    }








    [Table("EmployeeAttendence")]
    public  class EmployeeAttendence
    {
        [Key]
        public long Id { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public System.DateTime InTime { get; set; }
        public Nullable<System.DateTime> OutTime { get; set; }
        public Nullable<double> TotalActiveHours { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public int EmpId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        [ForeignKey("EmpId")]
        public virtual Employee Employee { get; set; }
    }




    [Table("EmployeeBankAccount_Tbl")]
    public  class EmployeeBankAccount_Tbl
    {
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSC { get; set; }
        public string MICR { get; set; }
        public string AccountType { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
    }

    [Table("EmployeeIncentivePetrol_tbl")]
    public  class EmployeeIncentivePetrol_tbl
    {
        [Key]
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int RoleTypeId { get; set; }
        public double IncentiveAmount { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public double PetrolAmount { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        [ForeignKey("RoleTypeId")]
        public virtual RoleType RoleType { get; set; }
    }



    [Table("EmployeeReaction_tbl")]
    public  class EmployeeReaction_tbl
    {
        [Key]
        public int Id { get; set; }
        public int TaskId { get; set; }
        public bool EmpAccepted { get; set; }
        public Nullable<System.DateTime> ReactionDateTime { get; set; }
        public Nullable<int> RejectionReasonId { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<bool> IsReassigned { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        [ForeignKey("TaskId")]
        public virtual TaskAssignment_tbl TaskAssignment_tbl { get; set; }

    }



    [Table("EmployeeRejactionReason")]

    public  class EmployeeRejactionReason
    {
        [Key]
        public int Id { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDateTime { get; set; }
    }





    [Table("EmployeeTakeOver_tbl")]
    public  class EmployeeTakeOver_tbl
    {
      [Key]
        public int Id { get; set; }
        public int TaskId { get; set; }
        public System.DateTime TakeoverDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int PrepareId { get; set; }

        [ForeignKey("TaskId")]
        public virtual TaskAssignment_tbl TaskAssignment_tbl { get; set; }


    }


    [Table("EmpTask")]

    public  class EmpTask
    {   [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int EmpId { get; set; }
        public string TaskStatus { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        [ForeignKey("EmpId")]
        public virtual Employee Employee { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order_tbl Order_tbl { get; set; }
    }




    [Table("Feedback")]
    public  class Feedback
    {
        [Key]
        public long FeedbackId { get; set; }
        public long UserId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifyBy { get; set; }
        public string Comments { get; set; }
    }




    [Table("feedback_title")]
    public  class feedback_title
    {
        [Key]
        public string fd_title { get; set; }
        public string fd_subtitle { get; set; }
        public int Id { get; set; }
    }




    [Table("Footer_content")]
    public  class Footer_content
    {
        [Key]
        public int id { get; set; }
        public string title1 { get; set; }
        public string content1 { get; set; }
        public string title2 { get; set; }
        public string content2 { get; set; }
        public string title3 { get; set; }
        public string content3 { get; set; }
        public string title4 { get; set; }
        public string content4 { get; set; }
        public string copyright { get; set; }
    }






    [Table("LaundryBag_Tbl")]
    public  class LaundryBag_Tbl
    {
        [Key]
        public int Id { get; set; }
        public string MaterialName { get; set; }
        public string BagImgUrl { get; set; }
        public int weightCapacity { get; set; }
        public int CountCapacity { get; set; }
        public int Price { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }





    [Table("Mail_collection")]
    public  class Mail_collection
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }


    [Table("ManageReferral")]
    public  class ManageReferral
    {
        [Key]
        public int Id { get; set; }
        public int BranchId { get; set; }
        public string ReferralAmount { get; set; }
        public string ValidityDays { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
    }





    [Table("ManageVendorService")]
    public  class ManageVendorService
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int ServiceId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public virtual Vendor Vendor { get; set; }
    }


    [Table("Offer")]
    public  class Offer
    {
        [Key]
        public int Id { get; set; }
        public string OfferTitle { get; set; }
        public string OfferContent { get; set; }
        public string OfferPrice { get; set; }
        public string OfferImage { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }



    [Table("Order_tbl")]
    public  class Order_tbl
    {
      
        [Key]
        public int Id { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<long> PickUpTimeSlotId { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public Nullable<long> DeliveryTimeSlotId { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<int> DeliveryTypeId { get; set; }
        public Nullable<int> ExpectedCount { get; set; }
        public Nullable<int> PaymentTypeId { get; set; }
        public int OrderTypeId { get; set; }
        public int PaymentStatusId { get; set; }
        public int OrderStatusId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string PromoCode { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public int PickUpAddressId { get; set; }
        public int DeliveryAddressId { get; set; }
        public bool UsedPkgBalance { get; set; }
        public Nullable<bool> UsedEsyWallet { get; set; }
        public Nullable<System.DateTime> Modified_date { get; set; }



        public virtual Address Address { get; set; }
 
        public virtual Branch Branch { get; set; }

        public virtual DeliveryType_tbl DeliveryType_tbl { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }
        public virtual OrderStatus_tbl OrderStatus_tbl { get; set; }
        public virtual OrderType_tbl OrderType_tbl { get; set; }

    }


    [Table("OrderCancel")]
    public  class OrderCancel
    {
        [Key]
        public int Id { get; set; }
        public int CancelById { get; set; }
        public int ReasonId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int OrderId { get; set; }
        public int RoleTypeId { get; set; }

        public virtual CancelReason CancelReason { get; set; }
        public virtual Order_tbl Order_tbl { get; set; }
        public virtual RoleType RoleType { get; set; }
    }


    [Table("OrderDelivery_tbl")]
    public  class OrderDelivery_tbl
    {
        [Key]
        public int Id { get; set; }
        public int TaskId { get; set; }
        public byte[] CustomerSign { get; set; }
        public System.DateTime DeliveryDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }

        public virtual TaskAssignment_tbl TaskAssignment_tbl { get; set; }

    }


    [Table("OrderDetail_tbl")]
    public  class OrderDetail_tbl
    {
     
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int PriceId { get; set; }
        public int Qunatity { get; set; }
        public double SubAmount { get; set; }
        public Nullable<int> Length { get; set; }
        public Nullable<int> Breadth { get; set; }
        public int CustomerId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

        public virtual Order_tbl Order_tbl { get; set; }
        public virtual PriceTable PriceTable { get; set; }

    }


    [Table("OrderProduct")]
    public  class OrderProduct
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int GenderId { get; set; }

        [ForeignKey("GenderId")]
        public virtual Gender Gender { get; set;}

    }


    [Table("OrderStatus_tbl")]
    public class OrderStatus_tbl
    {
     
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }


    }




    [Table("OrderType_tbl")]
    public  class OrderType_tbl
    {
   
        [Key]
        public int Id { get; set; }
        public string OrderType { get; set; }

    }

    [Table("Otptbl")]
    public  class Otptbl
    {
        [Key]
        public int Id { get; set; }
        public string Otp { get; set; }
        public string PhoneNumber { get; set; }
        public bool CheckOtp { get; set; }
    }


    [Table("PriceTable")]
    public  class PriceTable
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int BranchId { get; set; }
        public double Price { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public Nullable<int> UnitId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set;}
        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set;}
        [ForeignKey("UnitId")]
        public virtual Unit_Type Unit_Type { get; set;}
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set;}

    }


    [ComplexType]
    public  class Proc_BranchSalesTarget_Result
    {
        public int Id { get; set; }
        public string BranchTargetTotalSize { get; set; }
        public System.DateTime DateFrom { get; set; }
        public System.DateTime DateTo { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string Name { get; set; }
    }


    [ComplexType]
    public class Proc_ViewVendorPrice_Result
    {
        public  int Id { get; set; }
        public string ImgUrl { get; set; }
        public string ProductName { get; set; }
        public string ServiceName { get; set; }
        public double CustomerPrice { get; set; }
        public double VendorPrice { get; set; }
    }



    [ComplexType]
    public class Proc_GetPriceId_Result
    {
        public int id { get; set;}

    }


    [ComplexType]
    public  class Proc_CashTobeSubmitted_Result
    {
        public int Id { get; set; }
        public int FromEmpId { get; set; }
        public string EmployeeName { get; set; }
        public System.DateTime HandOverDate { get; set; }
        public double TotalAmount { get; set; }
        public Nullable<int> SubmitId { get; set; }
    }



    [ComplexType]
    public  class Proc_GetBranchInfo_Result
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNo { get; set; }
        public string EmailId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string CityName { get; set; }
    }



    [ComplexType]
    public  class Proc_GetCancelReasonRoleType_Result
    {
        public int Id { get; set; }
        public string Reason { get; set; }
    }





    [ComplexType]
    public  class Proc_GetCustomerInProcessOrderBranchWise_Result
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpTime { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryTime { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
    }

    [ComplexType]
    public  class Proc_GetCustomerOrderBranchWise_Result
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string CustomerName { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string PickUpAddress { get; set; }
        public string PickUpTime { get; set; }
        public Nullable<System.DateTime> PickUpDate { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryTime { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
    }






    [ComplexType]
    public  class Proc_GetCustomerTicket_Result
    {
        public int Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Message { get; set; }
        public string Bname { get; set; }
        public string CustName { get; set; }
        public string EmailId { get; set; }
        public string Com_Category { get; set; }
        public System.DateTime CreateDate { get; set; }
    }

    [ComplexType]
    public  class Proc_GetEmpAddress_Result
    {
        public int EmployeeId { get; set; }
        public string AddressType { get; set; }
        public string CityName { get; set; }
        public string AreaId { get; set; }
        public string HouseNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
    }

    [ComplexType]
    public  class Proc_GetEmployee_Result
    {
        public double BasicSalary { get; set; }
        public string emp_name { get; set; }
        public string password { get; set; }
        public int emp_id { get; set; }
        public System.DateTime DOJ { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string RoleName { get; set; }
        public string Name { get; set; }
    }




    [ComplexType]

    public  class Proc_GetEmployeebyRoleType_Result
    {
        public int emp_id { get; set; }
        public string emp_name { get; set; }
    }





    [ComplexType]
    public  class Proc_GetEmpTicket_Result
    {
        public string Name { get; set; }
        public string Com_Category { get; set; }
        public string EmpName { get; set; }
        public int Id { get; set; }
        public string EmailId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }


    [ComplexType]
    public  class Proc_GetEmpTicketcon_Result
    {
        public string outbox { get; set; }
        public string Inbox { get; set; }
        public Nullable<int> TicketId { get; set; }
        public Nullable<int> EmpId { get; set; }
    }



    [ComplexType]
    public  class Proc_GetGender_Result
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }



    [ComplexType]
    public  class Proc_GetLeads_Result
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string CustomerName { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> DateFrom { get; set; }
        public Nullable<System.DateTime> DateTo { get; set; }
        public string SalesTarget { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CallingDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.TimeSpan> CallingTime { get; set; }
        public string CallingStatus { get; set; }
    }




    [ComplexType]
    public  class Proc_GetOrders_Result
    {
        public Nullable<int> BranchId { get; set; }
        public string OrderId { get; set; }
    }








    [ComplexType]
    public  class Proc_GetPurchaseMethod_Result
    {
        public int Id { get; set; }
        public string PaymentType { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Description { get; set; }
    }





    [ComplexType]
    public  class Proc_GetReportingManager_Result
    {
        public string emp_name { get; set; }
        public string per_phoneNo { get; set; }
    }



    [ComplexType]
    public  class Proc_GetVendor_Result
    {
        public int Id { get; set; }
        public string VendorName { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> DOJ { get; set; }
        public Nullable<System.DateTime> DateOfContract { get; set; }
        public string Name { get; set; }
    }


    [ComplexType]
    public  class Proc_GetVendorCon_Result
    {
        public int Id { get; set; }
        public string OutBox { get; set; }
        public string Inbox { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<int> TicketId { get; set; }
    }


    [Table("Promo_Type")]
    public  class Promo_Type
    {
        [Key]
        public int Id { get; set; }
        public string P_type { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }



    [Table("ReferalAmount_Tbl")]
    public  class ReferalAmount_Tbl
    {

        [Key]
        public int Id { get; set; }
        public int Refer_Roleid { get; set; }
        public int Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        [ForeignKey("Refer_Roleid")]
        public virtual ReferRoleType ReferRoleType { get; set; }

    }

    [Table("Locality")]
    public class Locality
    {

        [Key]
        public int Id { get; set; }
        public int CityId { get; set; }
        public string LocalArea { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }

    }


    [Table("ReferenceType_Tbl")]
    public  class ReferenceType_Tbl
    {
        [Key]
        public int Id { get; set; }
        public string ReferenceType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
    }






    [Table("ReferRoleType")]
    public  class ReferRoleType
    {     
          [Key]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }




    [Table("RegisteredCustomerFeedback")]
    public  class RegisteredCustomerFeedback
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public Nullable<System.DateTime> FeedbackDate { get; set; }
        public string Message { get; set; }
        public Nullable<int> RatingCounts { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
    }









    [Table("RoleType")]
    public  class RoleType
    {  
        [Key]
        public int Id { get; set; }
        public string RoleName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

    }



    [Table("SalaryDetails")]
    public  class SalaryDetails
    {
        [Key]
        public int Id { get; set; }
        public int EmpId { get; set; }
        public double BasicSalary { get; set; }
        public double TA { get; set; }
        public double DA { get; set; }
        public double HRA { get; set; }
        public System.DateTime CreateDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("EmpId")]
        public virtual Employee Employee { get; set; }
    }





    [Table("Service")]
    public  class Service
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }

    }


    [Table("service_page_stripe")]
    public  class service_page_stripe
    {
        [Key]
        public int id { get; set; }
        public string box_image { get; set; }
        public string box_heading { get; set; }
        public string box_content { get; set; }
        public string read_more_link { get; set; }
    }




    [Table("ServiceCategory")]
    public  class ServiceCategory
    {
        [Key]
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }



    [Table("ServiceRequired_tbl")]
    public  class ServiceRequired_tbl
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> ServiceId { get; set; }

        public virtual Service Service { get; set; }
        public virtual Service Service1 { get; set; }
    }


    [Table("ServiceType")]
    public  class ServiceType
    {
        [Key]
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string ServiceType1 { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

        public virtual ServiceCategory ServiceCategory { get; set; }
    }




    [ComplexType]
    public  class SP_CollectionPending_Result
    {
        public int TaskId { get; set; }
        public int OrderId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public System.DateTime CollectedDate { get; set; }
        public string TaskType { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public double TotalAmount { get; set; }
    }




    [ComplexType]
    public class SP_EmployeeCashPending_Result
    {
        public int PendingId { get; set; }
        public int TaskId { get; set; }
        public int OrderId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public System.DateTime CreateDate { get; set; }
        public double TotalAmount { get; set; }
    }



    [ComplexType]
    public  class SP_EmployeeRejection_Result
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public int OrderId { get; set; }
        public Nullable<System.DateTime> ReactionDateTime { get; set; }
        public string Reason { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public string Emp_Name { get; set; }
        public string Type { get; set; }
    }






    [ComplexType]
    public  class SP_GetBankCashSubmisssionStatus_Result
    {
        public int RequestId { get; set; }
        public System.DateTime SubmitDate { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public double ToTalAmount { get; set; }
        public string BankRefNo { get; set; }
        public string RequestStaus { get; set; }
        public string ReviewerName { get; set; }
        public string RejectionReason { get; set; }
        public Nullable<System.DateTime> ReviewDateTime { get; set; }
    }



    [ComplexType]
    public  class SP_GetDeliveryTime_Result
    {
        public string FromTime { get; set; }
        public string ToTime { get; set; }
    }




    [ComplexType]
    public  class SP_GetEmployeeAttendance_Result
    {
        public long AttendenceId { get; set; }
        public int EmployeeId { get; set; }
        public string AttendenceDate { get; set; }
        public System.DateTime InTime { get; set; }
        public Nullable<System.DateTime> OutTime { get; set; }
        public Nullable<double> TotalActiveHours { get; set; }
        public string Remarks { get; set; }
        public string EmployeName { get; set; }
        public string AStatus { get; set; }
    }




    [ComplexType]

    public  class SP_GetEmployeeEarning_Result
    {
        public Nullable<System.DateTime> OnDate { get; set; }
        public Nullable<double> TotalIncentiveAmount { get; set; }
        public Nullable<double> TotalDistanceAmount { get; set; }
    }



    [Table("subscribe_content")]
    public  class subscribe_content
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string main_content { get; set; }
        public string back_image { get; set; }
        public string icon1 { get; set; }
        public string icon2 { get; set; }
        public string icon3 { get; set; }
        public string heading1 { get; set; }
        public string heading2 { get; set; }
        public string heading3 { get; set; }
        public string content1 { get; set; }
        public string content2 { get; set; }
        public string content3 { get; set; }
    }





    [Table("Subscription")]
    public  class Subscription
    {
        [Key]
        public long SubscriptionId { get; set; }
        public long EditionId { get; set; }
        public Nullable<int> UserLimitation { get; set; }
        public Nullable<int> YearlyPlan { get; set; }
        public Nullable<decimal> YearlyAmount { get; set; }
        public Nullable<int> MonthlyPlan { get; set; }
        public Nullable<decimal> MonthlyAmount { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifyBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }




    [Table("Surge")]
    public  class Surge
    {
        [Key]
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int AreaId { get; set; }
        public string SurgeAmount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdelete { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }

        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
    }


    [Table("Task")]
    public  class Task
    {
        [Key]
        public long TaskId { get; set; }
        public Nullable<long> UserId { get; set; }
        public string TaskSubject { get; set; }
        public Nullable<long> TaskAssign { get; set; }
        public Nullable<System.DateTime> TaskDate { get; set; }
        public string TaskDescription { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifyDate { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifyBy { get; set; }
    }










    [Table("TaskAssignment_tbl")]
    public  class TaskAssignment_tbl
    {
       [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int EmpId { get; set; }
        public Nullable<int> VendorId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Isdeleted { get; set; }
        public Nullable<System.DateTime> Createdate { get; set; }
        public int TaskTypeId { get; set; }
        public System.DateTime AssignmentDateTime { get; set; }
        public int AssignedById { get; set; }
        public Nullable<int> PrepareId { get; set; }

    }




    [Table("TaskType")]
    public  class TaskType
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }





    [Table("Tax_tbl")]
    public  class Tax_tbl
    {
        [Key]
        public int Id { get; set; }
        public string TaxName { get; set; }
        public double TaxPercentage { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }




    [Table("testimonial")]
    public  class testimonial
    {
        [Key]
        public int tesit_id { get; set; }
        public string title { get; set; }
        public string sub_title { get; set; }
        public string testi_content { get; set; }
        public string image_path { get; set; }
    }


    [Table("TimeSlot")]
    public  class TimeSlot
    {
        [Key]
        public long id { get; set; }
        public int BranchId { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public int TaskLimit { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get;  set;}
    }

    [Table("Transaction_tbl")]
    public  class Transaction_tbl
    {

        [Key]
        public int Id { get; set; }
        public Nullable<int> WalletId { get; set; }
        public string Trans_Type { get; set; }
        public Nullable<double> Amount { get; set; }
        public Nullable<double> Closing_Bal { get; set; }
        public Nullable<int> CashbckId { get; set; }
        public Nullable<int> RcrhgId { get; set; }
        public Nullable<int> OrderId { get; set; }
        public Nullable<System.DateTime> Trans_Date { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public virtual Order_tbl Order_tbl { get; set; }

    }


    [Table("Unit_Type")]
    public  class Unit_Type
    {
         [Key]
        public int Id { get; set; }
        public string U_Type { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }




    [Table("UpperFooter")]
    public  class UpperFooter
    {
        [Key]
        public int Id { get; set; }
        public string Tiitle { get; set; }
        public string SubTitle { get; set; }
        public string Icon { get; set; }
        public string Heading { get; set; }
        public string Content { get; set; }
        public string ContactNumber { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }





    [Table("Vendor")]
    public  class Vendor
    {

        [Key]
        public int Id { get; set; }
        public string VendorName { get; set; }
        public string Password { get; set; }
        public string PhoneNo { get; set; }
        public int AssignBranchId { get; set; }
        public string UploadPhoto { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string Per_PhoneNo { get; set; }
        public Nullable<System.DateTime> DOJ { get; set; }
        public Nullable<System.DateTime> DateOfContract { get; set; }
        public string ServiceTaxNo { get; set; }
        public int SecretPin { get; set; }
        public string Email { get; set; }
        [ForeignKey("AssignBranchId")]
        public virtual Branch Branch { get; set; }
    }






    [Table("VendorAddress")]
    public  class VendorAddress
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int AddTypeId { get; set; }
        public int CityId { get; set; }
        public string AreaId { get; set; }
        public string House_ShopNo { get; set; }
        public string StreetName { get; set; }
        public string LandMark { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        [ForeignKey("AddTypeId")]
        public virtual Address_type Address_type { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }

    }






    [Table("VendorBankAccount_Tbl")]
    public  class VendorBankAccount_Tbl
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSC { get; set; }
        public string MICR { get; set; }
        public string AccountType { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreateDate { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }


    }




    [Table("VendorPrice_tbl")]
    public  class VendorPrice_tbl
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int PriceId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<double> Price { get; set; }
        [ForeignKey("PriceId")]
        public virtual PriceTable PriceTable { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }
  
    }







    [Table("VendorService")]
    public  class VendorService
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string VendorName { get; set; }
        public int ServiceId { get; set; }
        public int VendorId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }






    [Table("VendorServiceDeliveryHour_tbl")]
    public partial class VendorServiceDeliveryHour_tbl
    {
        [Key]
        public int Id { get; set; }
        public int VendorId { get; set; }
        public int ServiceId { get; set; }
        public int DeliveryTypeId { get; set; }
        public int ServiceHours { get; set; }
        public int MarginalHours { get; set; }
        public int SurgePercentage { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        [ForeignKey("DeliveryTypeId")]
        public virtual DeliveryType_tbl DeliveryType_tbl { get; set; }
        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }
        [ForeignKey("VendorId")]
        public virtual Vendor Vendor { get; set; }

    }




    [Table("Welocme_area")]
    public  class Welocme_area
    {
        [Key]
        public int id { get; set; }
        public string welcome_content { get; set; }
        public string welcome_content1 { get; set; }
    }




    [Table("wlcm_srvic_stripe")]
    public  class wlcm_srvic_stripe
    {
        [Key]
        public int id { get; set; }
        public string box_image { get; set; }
        public string box_content { get; set; }
        public string redmore_link { get; set; }
    }


    [ComplexType]
    public  class SP_ViewPromoCode_Result
    {
        public int PromoCodeId { get; set; }
        public string Promo_code { get; set; }
        public string ExpiryDate { get; set; }
        public Nullable<int> RemainingCount { get; set; }
        public Nullable<int> MaxDisCount { get; set; }
        public string AppliedOn { get; set; }
        public string PromoType { get; set; }
        public Nullable<int> Percentage { get; set; }
        public Nullable<double> FlatAmount { get; set; }
    }


}