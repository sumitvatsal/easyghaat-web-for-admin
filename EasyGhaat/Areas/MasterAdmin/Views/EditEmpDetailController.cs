﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Views
{
    public class EditEmpDetailController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/EditEmpDetail
        public ActionResult Index(int id)
        {
            return View(db.Employees.Where(e=>e.emp_id==id).SingleOrDefault());
        }
    }
}