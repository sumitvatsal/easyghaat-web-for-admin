﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin
{
    public class ViewPromoCodeController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewPromoCode
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {




                return View(db.PromoCodes.Where(p => p.IsDeleted == false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");



            
        }
    }
}