﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin
{
    public class ManageRoleController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ManageRole
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {




                return View(db.RoleTypes.Where(r => r.IsActive == true && r.IsDeleted == false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");




        }



        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
                    RoleType obj = new RoleType();
                    obj.RoleName = frm["Role"];
                    obj.IsActive = true;
                    obj.IsDeleted = false;             
                    db.RoleTypes.Add(obj);
                    db.SaveChanges();
                    TempData["Message"] = "Successfull";
                    return RedirectToAction("Index");

        }
     
        



        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.RoleTypes.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if(obj!=null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
    
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        //For Edit
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(db.RoleTypes.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //Edit Testimonials
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            RoleType obj = new RoleType();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.RoleTypes.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.RoleName = frm["Role"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}