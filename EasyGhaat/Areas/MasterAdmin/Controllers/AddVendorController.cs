﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddVendorController : Controller
    {
        AdminContext db = new AdminContext();
        Vendor obj = new Vendor();
        VendorBankAccount_Tbl objVendorBankAccount_Tbl = new VendorBankAccount_Tbl();
        VendorAddress objVendorAddress = new VendorAddress();
        public int Valchk;
        public int _min = 1000;
        public int _max = 9999;

        // GET: MasterAdmin/AddVendor
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                ViewModel mymodel = new ViewModel();
                mymodel.State = db.States.ToList();
                mymodel.City = db.Cities.ToList();
                mymodel.Branch = db.Branches.Where(ab=>ab.IsDeleted==false && ab.IsActive==true).ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");


        }




        [HttpPost]
        public ActionResult Add(FormCollection frm, HttpPostedFileBase[] files)
        {
            try
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string relativePath = @"/Content/images" + pic;
                        obj.UploadPhoto = relativePath;
                        file.SaveAs(Server.MapPath(relativePath));
                        obj.AssignBranchId = Convert.ToInt32(frm["ddlBranch"]);
                        obj.VendorName = frm["Name"];
                        obj.Email = frm["EmailId"];
                        obj.Note = frm["Note"];
                        obj.Password = frm["Password"];
                        obj.PhoneNo = frm["PhoneNo"];
                        obj.Per_PhoneNo = frm["PermanentPhoneNo"];
                        obj.Note = frm["Note"];
                        obj.IsActive = true;
                        obj.ServiceTaxNo = frm["ServiceTaxNo"];
                        obj.DOJ = Convert.ToDateTime(frm["DOJ"]);              //DateTime.ParseExact(frm["DOJ"], "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                        obj.DateOfContract = Convert.ToDateTime(frm["DOC"]);  //DateTime.ParseExact(frm["DOC"], "MM/dd/yyyy", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat);
                        obj.IsDeleted = false;
                        TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                        DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                        obj.CreateDate = indianTime;
                        Random _rdm = new Random();
                        string m = _rdm.Next(_min, _max).ToString();
                        obj.SecretPin = Convert.ToInt32(m);
                        db.Vendors.Add(obj);
                        db.SaveChanges();
                        //Getting Current VendorId From databse Vendor Table
                        int VendorId = obj.Id;
                        //Fetchingf Record From db to check Vendor Record Exists or not
                        var ResultVendorRecord = db.Vendors.Where(e => e.Id == VendorId && e.IsDeleted == false && e.IsActive == true).SingleOrDefault();
                        if (ResultVendorRecord != null)
                        {
                            Valchk = Convert.ToInt32(frm["hdnchk"]);
                            if (Valchk == 1)
                            {
                                //Current Address
                                objVendorAddress.VendorId = ResultVendorRecord.Id;
                                objVendorAddress.AddTypeId = 4;
                                objVendorAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objVendorAddress.AreaId = frm["AreaIdCurrent"];
                                objVendorAddress.House_ShopNo = frm["HouseShopCurrent"];
                                objVendorAddress.StreetName = frm["StreetNameCurrent"];
                                objVendorAddress.LandMark = frm["LandMarkCurrent"];
                                objVendorAddress.Longitude = frm["LongitudeCurrent"];
                                objVendorAddress.Lattitude = frm["LattitudeCurrent"];
                                objVendorAddress.IsActive = true;
                                objVendorAddress.IsDeleted = false;
                                objVendorAddress.CreateDate = indianTime;
                                db.VendorAddresses.Add(objVendorAddress);
                                db.SaveChanges();
                                //Premanent Address
                                objVendorAddress.VendorId = ResultVendorRecord.Id;
                                objVendorAddress.AddTypeId = 5;
                                objVendorAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objVendorAddress.AreaId = frm["AreaIdCurrent"];
                                objVendorAddress.House_ShopNo = frm["HouseShopCurrent"];
                                objVendorAddress.StreetName = frm["StreetNameCurrent"];
                                objVendorAddress.LandMark = frm["LandMarkCurrent"];
                                objVendorAddress.Longitude = frm["LongitudeCurrent"];
                                objVendorAddress.Lattitude = frm["LattitudeCurrent"];
                                objVendorAddress.IsActive = true;
                                objVendorAddress.IsDeleted = false;
                                objVendorAddress.CreateDate = indianTime;
                                db.VendorAddresses.Add(objVendorAddress);
                                db.SaveChanges();
                            }

                            if (Valchk == 2)
                            {
                                //Current Address
                                objVendorAddress.VendorId = ResultVendorRecord.Id;
                                objVendorAddress.AddTypeId = 4;
                                objVendorAddress.CityId = Convert.ToInt32(frm["ddlCityCurrent"]);
                                objVendorAddress.AreaId = frm["AreaIdCurrent"];
                                objVendorAddress.House_ShopNo = frm["HouseShopCurrent"];
                                objVendorAddress.StreetName = frm["StreetNameCurrent"];
                                objVendorAddress.LandMark = frm["LandMarkCurrent"];
                                objVendorAddress.Longitude = frm["LongitudeCurrent"];
                                objVendorAddress.Lattitude = frm["LattitudeCurrent"];
                                objVendorAddress.IsActive = true;
                                objVendorAddress.IsDeleted = false;
                                objVendorAddress.CreateDate = indianTime;
                                db.VendorAddresses.Add(objVendorAddress);
                                db.SaveChanges();
                                //Premanent Address
                                objVendorAddress.VendorId = ResultVendorRecord.Id;
                                objVendorAddress.AddTypeId = 5;
                                objVendorAddress.CityId = Convert.ToInt32(frm["ddlCityPermanent"]);
                                objVendorAddress.AreaId = frm["AreaIdPermanent"];
                                objVendorAddress.House_ShopNo = frm["HouseShopCurrent"];
                                objVendorAddress.StreetName = frm["StreetNamePermanent"];
                                objVendorAddress.LandMark = frm["LandMarkPermanent"];
                                objVendorAddress.Longitude = frm["LongitudePermanent"];
                                objVendorAddress.Lattitude = frm["LattitudePermanent"];
                                objVendorAddress.IsActive = true;
                                objVendorAddress.IsDeleted = false;
                                objVendorAddress.CreateDate = indianTime;
                                db.VendorAddresses.Add(objVendorAddress);
                                db.SaveChanges();
                            }
                        }

                        //Indert into Vendor Bank Detail
                        objVendorBankAccount_Tbl.VendorId = ResultVendorRecord.Id;
                        objVendorBankAccount_Tbl.BankName = frm["BankName"];
                        objVendorBankAccount_Tbl.IFSC = frm["IFSCCode"];
                        objVendorBankAccount_Tbl.MICR = frm["MICR"];
                        objVendorBankAccount_Tbl.BankAccountNumber = frm["BankAccountNo"];
                        objVendorBankAccount_Tbl.BranchName = frm["BranchName"];
                        int AcTypeId = Convert.ToInt32(frm["ddlAccountType"]); 
                        if(AcTypeId==1)
                        {
                            objVendorBankAccount_Tbl.AccountType = "Saving";

                        }
                        if (AcTypeId == 2)
                        {
                            objVendorBankAccount_Tbl.AccountType = "Current";

                        }
                        objVendorBankAccount_Tbl.IsDeleted = false;
                        objVendorBankAccount_Tbl.CreateDate = indianTime;
                        db.VendorBankAccount_Tbl.Add(objVendorBankAccount_Tbl);
                        db.SaveChanges();
                        Session["VendorId"] = ResultVendorRecord.Id;

                    }
                }

                return RedirectToAction("../AssignServices/Index");

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }


        }

        [HttpGet]
        public JsonResult CheckPhoneNo(Vendor objVendor)
        {
            var Result = db.Vendors.Where(pc => pc.Per_PhoneNo == objVendor.Per_PhoneNo && pc.IsActive == true && pc.IsDeleted == false).SingleOrDefault();
            if (Result != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult CheckEmailId(Vendor objVendor)
        {
            var Result = db.Vendors.Where(pc => pc.Email == objVendor.Email && pc.IsActive == true && pc.IsDeleted == false).SingleOrDefault();
            if (Result != null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }





    }
}