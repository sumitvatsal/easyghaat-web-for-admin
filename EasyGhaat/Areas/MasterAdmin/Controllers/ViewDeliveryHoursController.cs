﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewDeliveryHoursController : Controller
    {
        AdminContext db = new AdminContext();
        DeliveryHours_tbl obj = new DeliveryHours_tbl();

        // GET: MasterAdmin/ViewDeliveryHours
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.DeliveryHours_tbl.Where(d=>d.IsDeleted==false).ToList());
            }
            return RedirectToAction("../../AdminLogin/Index");

        }






        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.DeliveryHours_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }







    }
}