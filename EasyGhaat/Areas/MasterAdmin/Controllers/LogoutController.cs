﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class LogoutController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/Logout
        public ActionResult Index()
        {
            int AdminSessionId = Convert.ToInt32(Session["AdminId"]);
            int SessionId = Convert.ToInt32(Session["SessionId"]);
            if (SessionId != 0 && AdminSessionId!=0)
            {
                EmployeeSession obj = new EmployeeSession();
                obj = db.EmployeeSessions.Where(a => a.Id == SessionId).SingleOrDefault();
                if (obj != null)
                {
                    obj.Id = SessionId;
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    obj.LogOut_DateTime = indianTime;
                    db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    Session.Abandon();
                    Session.RemoveAll();
                }
                return RedirectToAction("../../AdminLogin/Index");
            }
            else
            {
                return RedirectToAction("../../AdminLogin/Index");

            }               
   
        }
    }
}