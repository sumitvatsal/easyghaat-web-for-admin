﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdatePriceListController : Controller
    {
        AdminContext db = new AdminContext();
        AdminContext dbAdminContext = new AdminContext();
        // GET: MasterAdmin/UpdatePriceList
        public ActionResult Index(int id)
        {
            using (AdminContext db = new AdminContext())
            {
                int SessionId = Convert.ToInt32(Session["AdminId"]);

                if (SessionId != 0)
                {
                    return View(db.VendorPrice_tbl.Where(vp => vp.VendorId == id).ToList());
                }

                return RedirectToAction("../../AdminLogin/Index");
  
            }

        }


        //Edit Product
     public string Edit(Product objProduct, FormCollection frm)
        {
            PriceTable objprice = new PriceTable();
            int Id = objProduct.Id;
            var obj = dbAdminContext.Products.Find(Id);
            string msg = "";
            
                if (obj!=null)
                {
                    obj.Id = objProduct.Id;
                    obj.Name = frm["ProductName"];
                    obj.Description = objProduct.Description;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Entry(obj).State = EntityState.Modified;
                    db.SaveChanges();          
                 }

            int PId = obj.Id;
            var obj1 = db.PriceTables.Where(p => p.ProductId.Equals(PId)).SingleOrDefault();
            if(obj1!=null)
                {
                obj1.Id = obj1.Id;
                obj1.Price = Convert.ToDouble(frm["Price"]);
                db.Entry(obj1).State = EntityState.Modified;
                db.SaveChanges();
            }

        return msg = "Saved Successfully";
        }




        //For Delete
        public string Delete(int id)
        {
            PriceTable objprice = new PriceTable();
            var obj = dbAdminContext.Products.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj.IsDeleted == false)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;
                obj.CreateDate = DateTime.UtcNow;
                db.SaveChanges();
            }
            return "Deleted successfully";
        }















    }
}