﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class VendorServiceDeliveryHourController : Controller
    {

        AdminContext db = new AdminContext();
        // GET: MasterAdmin/VendorServiceDeliveryHour
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                ViewModel mymodel = new ViewModel();
                mymodel.Vendor = db.Vendors.ToList();
                mymodel.DeliveryType_tbl = db.DeliveryType_tbl.ToList();
                mymodel.Service = db.Services.ToList();
                mymodel.Branch = db.Branches.ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");

        }

        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            VendorServiceDeliveryHour_tbl  obj = new VendorServiceDeliveryHour_tbl();
            var VendorId = int.Parse(frm["ddlVendor"]);
            var DeliveryTypeId = int.Parse(frm["ddlType"]);
            var ServiceId= int.Parse(frm["ddlService"]);
            var ob = db.VendorServiceDeliveryHour_tbl.Where(d => d.VendorId == VendorId && d.DeliveryTypeId == DeliveryTypeId && d.ServiceId == ServiceId && d.IsDeleted==false).SingleOrDefault();

            if (ob != null)
            {
                ViewData["Message-1"] = "Successfull";
                return RedirectToAction("Index");
            }

            else
            {
                obj.VendorId = int.Parse(frm["ddlVendor"]);
                obj.DeliveryTypeId = int.Parse(frm["ddlType"]);
                obj.ServiceId = int.Parse(frm["ddlService"]);
                obj.ServiceHours = int.Parse(frm["txtServiceHours"]);
                obj.MarginalHours = int.Parse(frm["txtMarginalHours"]);
                obj.SurgePercentage = int.Parse(frm["txtSurgePrice"]);
                obj.IsDeleted = false;
                obj.IsActive = true;
                obj.CreateDate = DateTime.UtcNow;
                db.VendorServiceDeliveryHour_tbl.Add(obj);
                db.SaveChanges();
            }
            ViewData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}