﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin
{
    public class AddWelcomeServiceController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/AddWelcomeService
        public ActionResult Index()
        {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {



                return View(db.wlcm_srvic_stripe
                    .ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");



        }

        public ActionResult Submit(HttpPostedFileBase[] files)
        {
            wlcm_srvic_stripe obj = new wlcm_srvic_stripe();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;
                    obj.box_image = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.box_content = Request.Form["BoxContent"];
                    obj.redmore_link = Request.Form["ReadMoreContent"];
                    db.wlcm_srvic_stripe.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }





        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.wlcm_srvic_stripe.Where(c => c.id.Equals(id)).SingleOrDefault();
            db.wlcm_srvic_stripe.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.wlcm_srvic_stripe.Where(c => c.id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            wlcm_srvic_stripe obj = new wlcm_srvic_stripe();
            obj.id = Convert.ToInt32(frm["hdnId"]);
            obj = db.wlcm_srvic_stripe.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(frm["hdnId"]);
                obj.box_content = frm["BoxContent"];
                obj.redmore_link = frm["ReadMoreContent"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }








    }
}