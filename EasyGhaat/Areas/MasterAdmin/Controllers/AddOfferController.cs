﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddOfferController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddOffer
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Offers.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


      
        }
        [HttpPost]
        public ActionResult Save(HttpPostedFileBase[] files)
        {       
            Offer obj = new Offer();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;
                    obj.OfferImage = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.OfferContent = Request.Form["OfferContent"];
                    obj.OfferPrice = Request.Form["OfferPrice"];
                    obj.OfferTitle = Request.Form["OfferTitle"];
                    db.Offers.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Offers.Where(c => c.Id.Equals(id)).SingleOrDefault();
            db.Offers.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        //For Edit
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(db.Offers.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //Edit Testimonials
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            Offer obj = new Offer();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Offers.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.OfferTitle = frm["OfferTitle"];
                obj.OfferContent = frm["OfferContent "];
                obj.OfferPrice = frm["OfferPrice "];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}