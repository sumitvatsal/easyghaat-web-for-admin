﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddPromoCodeController : Controller
    {
        AdminContext db = new AdminContext();
        ViewModel objviewmodel = new ViewModel();

        // GET: MasterAdmin/AddPromoCode
        public ActionResult Index()
        {
            try
            {



                int SessionId = Convert.ToInt32(Session["AdminId"]);

                if (SessionId != 0)
                {

                    objviewmodel.Applying_pros = db.Applying_pro.ToList();
                    objviewmodel.Promo_Type = db.Promo_Type.ToList();
                    return View(objviewmodel); ;
                }

                return RedirectToAction("../../AdminLogin/Index");


            }
            catch (Exception ex)

            {
                throw ex;
            }
        }


        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            PromoCode_Service obj1 = new PromoCode_Service();
            PromoCode obj = new PromoCode();

            try
            {

                int first = Convert.ToInt32(frm["chkWash&Iron"]);
                int second = Convert.ToInt32(frm["chkSteamIron"]);
                int third = Convert.ToInt32(frm["chkPremiumWash"]);
                int fourth = Convert.ToInt32(frm["chkDryClean"]);
                int fivth = Convert.ToInt32(frm["chkOnlyIron"]);
                int AppliedId = Convert.ToInt32(frm["ddlAppliedOn"]);
                if (AppliedId == 1)
                {

                    if (first == 0 && second == 0 && third == 0 && fourth == 0 && fivth == 0)
                    {
                        TempData["ChkMessage"] = "please select";
                        return RedirectToAction("Index");
                    }
                }
                obj.Promo_Code = frm["txtPromoCode"];
                obj.MaxUsers = Convert.ToInt32(frm["MaxUsers"]);
                obj.ExpiryDate = Convert.ToDateTime(frm["ExpiryDate"]);
                obj.MinOrder = Convert.ToDouble(frm["MinOrder"]);
                obj.PromoType = Convert.ToInt32(frm["ddlPromoType"]);
                var value = obj.PromoType;
                if (value == 2)
                {
                    obj.FlatAmount = Convert.ToDouble(frm["FlatAmount"]);
                }
                if (value == 1)
                {
                    if (frm["Percentage"] == "null")
                    {                                    
                        TempData["Percentage"] = "Successfull";
                        return RedirectToAction("Index");
                    }
                    else
                    {

                        obj.Percentage = Convert.ToInt32(frm["Percentage"]);
                    }

                    int md  = Convert.ToInt32(frm["MaxDiscount"]);

                    if (md ==0)
                    {
                        TempData["MaxDiscount"] = "Successfull";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        obj.MaxDiscount = Convert.ToInt32(frm["MaxDiscount"]);
                    }
                }
                obj.IsActive = true;
                obj.IsDeleted = false;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                obj.UsedCount = 0;
                obj.AppliedId = Convert.ToInt32(frm["ddlAppliedOn"]);
                db.PromoCodes.Add(obj);
                db.SaveChanges();
                if (AppliedId == 1)
                {

                    int PromoId = obj.Id;
                    if (first != 0)
                    {
                        obj1.ServiceId = Convert.ToInt32(frm["chkWash&Iron"]);
                        obj1.PromoCode_Id = PromoId;
                        obj1.IsDeleted = false;
                        obj1.IsActive = true;
                        obj.CreateDate = indianTime;
                        db.PromoCode_Service.Add(obj1);
                        db.SaveChanges();
                    }
                    if (second != 0)
                    {

                        obj1.ServiceId = Convert.ToInt32(frm["chkSteamIron"]);
                        obj1.PromoCode_Id = PromoId;
                        obj1.IsDeleted = false;
                        obj1.IsActive = true;
                        obj.CreateDate = indianTime;
                        db.PromoCode_Service.Add(obj1);
                        db.SaveChanges();
                    }
                    if (third != 0)
                    {
                        obj1.ServiceId = Convert.ToInt32(frm["chkPremiumWash"]);
                        obj1.PromoCode_Id = PromoId;
                        obj1.IsDeleted = false;
                        obj1.IsActive = true;
                        obj.CreateDate = indianTime;
                        db.PromoCode_Service.Add(obj1);
                        db.SaveChanges();
                    }

                    if (fourth != 0)
                    {

                        obj1.ServiceId = Convert.ToInt32(frm["chkDryClean"]);
                        obj1.PromoCode_Id = PromoId;
                        obj1.IsDeleted = false;
                        obj1.IsActive = true;
                        obj.CreateDate = indianTime;
                        db.PromoCode_Service.Add(obj1);
                        db.SaveChanges();
                    }

                    if (fivth != 0)
                    {
                        obj1.ServiceId = Convert.ToInt32(frm["chkOnlyIron"]);
                        obj1.PromoCode_Id = PromoId;
                        obj1.IsDeleted = false;
                        obj1.IsActive = true;
                        obj.CreateDate = indianTime;
                        db.PromoCode_Service.Add(obj1);
                        db.SaveChanges();
                    }
                }
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }

            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public JsonResult CheckPromoCode(PromoCode objPromoCode)
        {            
                var Result = db.PromoCodes.Where(pc => pc.Promo_Code == objPromoCode.Promo_Code && pc.IsActive == true && pc.IsDeleted == false).SingleOrDefault();
                if (Result != null)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);          
        }










    }
}