﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class EditVendorPriceListController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/EditVendorPriceList
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }




        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.PriceTables.Where(pc => pc.Id == id).SingleOrDefault());
        }



        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            PriceTable obj = new PriceTable();
            int Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.PriceTables.Find(Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.Price = Convert.ToDouble(frm["Price"]);
                obj.IsActive = true;
                obj.IsDeleted = false;  
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }     
            return RedirectToAction("../ViewVendorPrice/Search");
        }





    }
}