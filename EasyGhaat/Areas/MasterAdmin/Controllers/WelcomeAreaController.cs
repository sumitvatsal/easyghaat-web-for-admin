﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class WelcomeAreaController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/WelcomeArea
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {



                return View(db.Welocme_area.SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");


     
        }



        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            
            Welocme_area obj = new Welocme_area();
            obj.welcome_content = form["WelcomeContent"].ToString();
            obj.welcome_content1 = form["WelcomeContent2"].ToString();
            db.Welocme_area.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");

            //foreach (HttpPostedFileBase file in files)
            //{
            //    if (file != null)
            //    {
            //        string pic = System.IO.Path.GetFileName(file.FileName);
            //        string relativePath = @"~\img\" + pic;
            //        if (obj.image_path1!=null && obj.image_path2 ==null)
            //        { 
            //        obj.image_path2 = relativePath;
            //        file.SaveAs(Server.MapPath(relativePath));
            //        }
            //       else  if (obj.image_path2 != null && obj.image_path3 == null)
            //        {
            //            obj.image_path3 = relativePath;
            //            file.SaveAs(Server.MapPath(relativePath));
            //        }
            //        else if (obj.image_path3 != null && obj.image_path4 == null)
            //        {
            //            obj.image_path4 = relativePath;
            //            file.SaveAs(Server.MapPath(relativePath));
            //        }
            //        else
            //        {
            //            obj.image_path1 = relativePath;
            //            file.SaveAs(Server.MapPath(relativePath));
            //        }                 
            //    }
            //}
            //file.SaveAs(LocalPath);    
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult UpdateWelcomeArea(FormCollection frm)
        {
            Welocme_area obj = new Welocme_area();
            obj.id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Welocme_area.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(frm["hdnId"]);
                obj.welcome_content = frm["WelcomeContent"];
                obj.welcome_content1 = frm["WelcomeContent2"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }






    }
}






