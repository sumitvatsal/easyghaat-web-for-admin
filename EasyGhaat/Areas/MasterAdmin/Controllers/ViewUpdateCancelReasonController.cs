﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewUpdateCancelReasonController : Controller
    {
        AdminContext db = new AdminContext();
        ViewModel mymodel = new ViewModel();
        // GET: MasterAdmin/ViewUpdateCancelReason
        public ActionResult Index()
        {
            try
            {


                int SessionId = Convert.ToInt32(Session["AdminId"]);

                if (SessionId != 0)
                {




                    mymodel.RoleType = db.RoleTypes.ToList();
                    return View(mymodel);
                }

                return RedirectToAction("../../AdminLogin/Index");

            }
            catch (Exception ex)

            {
                throw ex;
            }
        }



        [HttpPost]
        public JsonResult UpdateCancelReason(CancelReason objCancelReason)
        {
            try
            {
                var Result = db.CancelReasons.Find(objCancelReason.Id);
                if (Result != null)
                {
                    Result.Id = objCancelReason.Id;
                    Result.Reason = objCancelReason.Reason;              
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }


        public JsonResult DeleteCancelReason(CancelReason objCancelReason)
        {
            try
            {
                var Result = db.CancelReasons.Find(objCancelReason.Id);
                if (Result != null)
                {
                    Result.Id = objCancelReason.Id;
                    Result.IsDeleted = true;
                    Result.IsActive = false;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }


    }
}