﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateFooterController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateFooter
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Footer_content.SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");


        }





        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Submit(FormCollection frm)
        {
            Footer_content obj = new Footer_content();
            obj.id = Convert.ToInt32(frm["hdnId"]);  
            obj.title1 = frm["title1"];
            obj.title2 = frm["title2"];
            obj.title3 = frm["title3"];
            obj.title4 = frm["title4"];
            obj.content1 = frm["content1"];
            obj.content2 = frm["content2"];
            obj.content3 = frm["content3"];
            obj.content4 = frm["content4"];
           
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }
    }
}