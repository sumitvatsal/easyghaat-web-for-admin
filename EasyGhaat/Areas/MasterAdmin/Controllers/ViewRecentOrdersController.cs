﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewRecentOrdersController : Controller
    {

        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewRecentOrders
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {



                ViewModel viewmodel = new ViewModel();
                viewmodel.Branch = db.Branches.ToList();
                return View(viewmodel);
            }

            return RedirectToAction("../../AdminLogin/Index");


        }


        [HttpGet]
        public JsonResult GetCustomerOrders(string sidx, string sord, int page, int rows, int ddlBranch = 0)
        {
            using (AdminContext db = new AdminContext())
            {
                int pageIndex = Convert.ToInt32(page) - 1;
                Session["BranchId"] = ddlBranch;
                int pageSize = rows;
                var obj = db.Database.SqlQuery<Proc_GetCustomerOrderBranchWise_Result>("Proc_GetCustomerOrderBranchWise  @BranchId",
                new SqlParameter("@BranchId", ddlBranch)
                ).ToList<Proc_GetCustomerOrderBranchWise_Result>();

                int totalRecords = obj.Count();
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
                if (sord.ToUpper() == "DESC")
                {
                    obj = obj.OrderByDescending(s => s.Id).ToList();
                    obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                else
                {
                    obj = obj.OrderBy(s => s.Id).ToList();
                    obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
                }
                var jsonData = new
                {
                    total = totalPages,
                    page,
                    records = totalRecords,
                    rows = obj
                };
                return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
        }



    }
}