﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.Entity.Validation;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AboutUsController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AboutUs
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if(SessionId!=0)
            {

                return View(db.about_stripe.SingleOrDefault());
            }
         
         return RedirectToAction("../../AdminLogin/Index");

            




        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            about_stripe obj = new about_stripe();
            obj.about_content = form["LeftSideContent"].ToString();
            obj.RightSideContent = form["RightSideContent"].ToString();
            obj.ReadMore_Content = form["ReadMoreContent"].ToString();
            db.about_stripe.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(FormCollection fm)
        {
            about_stripe obj = new about_stripe();
            obj.id = Convert.ToInt32(fm["hdnId"]);
            obj = db.about_stripe.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(fm["hdnId"]);
                obj.about_content = fm["LeftSideContent"].ToString();
                obj.RightSideContent = fm["RightSideContent"].ToString();
                obj.ReadMore_Content = fm["ReadMoreContent"].ToString();
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }
    }
}

    
