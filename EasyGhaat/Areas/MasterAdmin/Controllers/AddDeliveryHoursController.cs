﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddDeliveryHoursController : Controller
    {
        AdminContext db = new AdminContext();
        DeliveryHours_tbl obj = new DeliveryHours_tbl();

        // GET: MasterAdmin/AddDeliveryHours
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {
                ViewModel mymodel = new ViewModel();
                mymodel.Branch = db.Branches.ToList();
                mymodel.Service = db.Services.ToList();
                mymodel.DeliveryType_tbl = db.DeliveryType_tbl.ToList();
                return View(mymodel);
            }
            return RedirectToAction("../../AdminLogin/Index");
        }






        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            try
            {
                int BranchId = int.Parse(frm["ddlBranch"]);
                int ServiceId = int.Parse(frm["ddlService"]);
                int DeliveryTypeId = int.Parse(frm["ddlType"]);
                int DeliveryHours= int.Parse(frm["txtDeliveryHours"]);
               var objCheck = db.DeliveryHours_tbl.Where(dh => dh.BranchId == BranchId && dh.ServiceId == ServiceId && dh.DeliveryTypeId == DeliveryTypeId && dh.IsDeleted == false && dh.DeliveryHours== DeliveryHours).SingleOrDefault();
                if(objCheck != null)
                {
                    TempData["Message1"] = "Successfull";
                    return RedirectToAction("Index");
                }
                obj.BranchId = int.Parse(frm["ddlBranch"]);
                obj.ServiceId = int.Parse(frm["ddlService"]);
                obj.DeliveryTypeId = int.Parse(frm["ddlType"]);
                obj.DeliveryHours = int.Parse(frm["txtDeliveryHours"]);
                obj.MarginalHours = int.Parse(frm["txtMarginalHours"]);
                obj.Description = frm["txtrDescription"];
                obj.IsDeleted = false;
                db.DeliveryHours_tbl.Add(obj);
                db.SaveChanges();
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                TempData["Message-1"] = "UnSuccessfull";
                return RedirectToAction("Index");
            }
        }
    }
}