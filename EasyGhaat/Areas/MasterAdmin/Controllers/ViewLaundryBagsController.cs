﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewLaundryBagsController : Controller
    {
       private AdminContext db = new AdminContext();


        // GET: MasterAdmin/ViewLaundryBags
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {
              return View(db.LaundryBag_Tbl.Where(lb => lb.IsActive == true && lb.IsDeleted == false).OrderByDescending(o => o.Id).ToList());
            }
            return RedirectToAction("../../AdminLogin/Index");
          
        }






        //For Edit
        public ActionResult Edit(int id)
        {
            return View(db.LaundryBag_Tbl.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }





        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase[] files, FormCollection frm)
        {
            LaundryBag_Tbl obj = new LaundryBag_Tbl();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            int Price = Convert.ToInt32(frm["hdnPrice"]);
            int newPrice = Convert.ToInt32(frm["Price"]);
            if (Price == newPrice)
            {
                obj = db.LaundryBag_Tbl.Find(obj.Id);
                if (obj != null)
                {
                    foreach (HttpPostedFileBase file in files)
                    {
                        if (file != null)
                        {
                            string pic = System.IO.Path.GetFileName(file.FileName);
                            string relativePath = @"/Content/images" + pic;
                            obj.BagImgUrl = relativePath;
                            //file.SaveAs(LocalPath);
                            file.SaveAs(Server.MapPath(relativePath));
                            obj.Id = Convert.ToInt32(frm["hdnId"]);
                            obj.MaterialName = frm["Material"];
                            obj.weightCapacity = Convert.ToInt32(frm["weight"]);
                            obj.CountCapacity = Convert.ToInt32(frm["Count"]);
                            obj.Price = Convert.ToInt32(frm["Price"]);
                            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                            obj.ModifiedDate = indianTime;
                            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                        else
                        {
                            obj.Id = Convert.ToInt32(frm["hdnId"]);
                            obj.MaterialName = frm["Material"];
                            obj.weightCapacity = Convert.ToInt32(frm["weight"]);
                            obj.CountCapacity = Convert.ToInt32(frm["Count"]);
                            obj.Price = Convert.ToInt32(frm["Price"]);
                            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                            obj.ModifiedDate = indianTime;
                            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                    }
                }
            }

            else
            {
                obj = db.LaundryBag_Tbl.Find(obj.Id);
                var path = obj.BagImgUrl;
                if (obj != null)
                {
                    obj.Id = Convert.ToInt32(frm["hdnId"]);
                    obj.IsDeleted = true;
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    obj.ModifiedDate = indianTime;
                    db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

               foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string relativePath = @"/Content/images" + pic;
                        obj.BagImgUrl = relativePath;
                        //file.SaveAs(LocalPath);
                        file.SaveAs(Server.MapPath(relativePath));
                        obj.MaterialName = frm["Material"];
                        obj.weightCapacity = Convert.ToInt32(frm["weight"]);
                        obj.CountCapacity = Convert.ToInt32(frm["Count"]);
                        obj.IsDeleted = false;
                        obj.Price = Convert.ToInt32(frm["Price"]);
                        TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                        DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                        obj.ModifiedDate = indianTime;
                        db.LaundryBag_Tbl.Add(obj);
                        db.SaveChanges();
                    }

                    else
                    {                      
                        obj.BagImgUrl = path;
                        obj.MaterialName = frm["Material"];
                        obj.weightCapacity = Convert.ToInt32(frm["weight"]);
                        obj.CountCapacity = Convert.ToInt32(frm["Count"]);
                        obj.IsDeleted = false;
                        obj.Price = Convert.ToInt32(frm["Price"]);
                        TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                        DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                        obj.ModifiedDate = indianTime;
                        db.LaundryBag_Tbl.Add(obj);
                        db.SaveChanges();


                    }

                }
            }
           // TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.LaundryBag_Tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            //TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }






    }
}