﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddTimeSlotController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/AddTimeSlot
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);
            if (SessionId != 0)
            {
                ViewModel mymodel = new ViewModel();
                mymodel.Branch = db.Branches.ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");

        }

        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            TimeSlot obj = new TimeSlot();
            var FromTime = frm["ddlFromTime"];
            var ToTime= frm["ddlToTime"];
            int bid= Convert.ToInt32(frm["ddlBranch"]);
            var data = db.TimeSlots.Where(ts => ts.FromTime == FromTime && ts.ToTime == ToTime && ts.IsActive == true && ts.BranchId == bid).SingleOrDefault();   
            int Count = db.TimeSlots.Where(ts => ts.BranchId == bid && ts.IsActive == true).Count();
            int mx = 1;
            int ft = 2;
            var check = db.TimeSlots.FirstOrDefault (k => k.BranchId == bid && k.IsActive == true);
            if (check != null)
            {
                 var maxTime = db.TimeSlots.Where(s => s.BranchId == bid && s.IsActive==true).Select(r => r.ToTime).Max();
                 mx = Convert.ToInt32(maxTime.Substring(0, 2));
                 ft = Convert.ToInt32(FromTime.Substring(0, 2));
            }
            else
            {
                obj.BranchId = Convert.ToInt32(frm["ddlBranch"]);
                obj.FromTime = frm["ddlFromTime"];
                obj.ToTime = frm["ddlToTime"];
                obj.TaskLimit =  Convert.ToInt32(frm["TaskLimit"]);
                obj.IsDeleted = false;
                obj.IsActive = true;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                db.TimeSlots.Add(obj);
                db.SaveChanges();
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");
            }
            if (Count == 6)
            {
                TempData["Message-2"] = "Alredy Exists";
                return RedirectToAction("Index");
            }
            if (data != null)
            {
                TempData["Message-1"] = "Alredy Exists";
                return RedirectToAction("Index");
            }
            if (ft > mx)
            {
                obj.BranchId = Convert.ToInt32(frm["ddlBranch"]);
                obj.FromTime = frm["ddlFromTime"];
                obj.ToTime = frm["ddlToTime"];
                obj.TaskLimit = Convert.ToInt32(frm["TaskLimit"]);
                obj.IsDeleted = false;
                obj.IsActive = true;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                db.TimeSlots.Add(obj);
                db.SaveChanges();
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");
            }
            else if(ft == mx)
            {
                obj.BranchId = Convert.ToInt32(frm["ddlBranch"]);
                obj.FromTime = frm["ddlFromTime"];
                obj.ToTime = frm["ddlToTime"];
                obj.TaskLimit = Convert.ToInt32(frm["TaskLimit"]);
                obj.IsDeleted = false;
                obj.IsActive = true;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                obj.CreateDate = indianTime;
                db.TimeSlots.Add(obj);
                db.SaveChanges();
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");
            }
            else
            {

            }
            TempData["Message-3"] = "Alredy Exists";
            return RedirectToAction("Index");
        }
    }
}