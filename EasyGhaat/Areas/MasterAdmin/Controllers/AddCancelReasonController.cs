﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddCancelReasonController : Controller
    {
        AdminContext db = new AdminContext();
        ViewModel mymodel = new ViewModel();
        // GET: MasterAdmin/AddCancelReason
        public ActionResult Index()
        {
            try
            {


                int SessionId = Convert.ToInt32(Session["AdminId"]);

                if (SessionId != 0)
                {

                    mymodel.RoleType = db.RoleTypes.ToList();
                    return View(mymodel);
                }

                return RedirectToAction("../../AdminLogin/Index");


            }
            catch (Exception ex)

            {
                throw ex;
            }
        }



        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            CancelReason objCancelReason = new CancelReason();
            try
            {
                objCancelReason.Reason = frm["Reason"];
                objCancelReason.RoleId = Convert.ToInt32(frm["ddlRole"]);
                objCancelReason.IsActive = true;
                objCancelReason.IsDeleted = false;
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                objCancelReason.CreateDate = indianTime;
                db.CancelReasons.Add(objCancelReason);
                db.SaveChanges();
                TempData["Message"] = "Successfull";
                return RedirectToAction("Index");

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                TempData["Message-1"] = "UnSuccessfull";
                return RedirectToAction("Index");
            }

        }
    }
}