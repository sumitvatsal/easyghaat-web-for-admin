﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewPriceListController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewPriceList
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.PriceTables.Where(pt => pt.IsDeleted == false && pt.IsActive == true).Include(i=>i.Product).Include(i=>i.Service).Include(i=>i.Branch).ToList().OrderByDescending(pt => pt.Id));
            }

            return RedirectToAction("../../AdminLogin/Index");



           
        }




        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.PriceTables.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }







    }
}