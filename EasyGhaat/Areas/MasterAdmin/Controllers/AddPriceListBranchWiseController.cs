﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddPriceListBranchWiseController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/AddPriceListBranchWise
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.PriceTables.Where(b => b.BranchId == 2).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


        }










    }
}