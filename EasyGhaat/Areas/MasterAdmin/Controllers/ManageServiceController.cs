﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageServiceController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ManageService
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");

        }


        [HttpPost]
        public ActionResult Add(HttpPostedFileBase[] files, FormCollection frm)
        {
            Service obj = new Service();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;
                   // obj.ImageUrl = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.Name = frm["Name"];
                    obj.Description = frm["Description"];
                    obj.IsActive = true;
                    obj.IsDeleted = false;
                    obj.CreateDate = DateTime.UtcNow;
                    db.Services.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}