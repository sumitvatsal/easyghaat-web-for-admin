﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewDeliveryTypeSurgePriceController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewDeliveryTypeSurgePrice
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.DeliveryType_SurgePrice.Include(ab => ab.Branch).Include(ab => ab.DeliveryType_tbl).Where(ab=>ab.IsDeleted==false).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");



        }


        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.DeliveryType_SurgePrice.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }







        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            DeliveryType_SurgePrice obj = new DeliveryType_SurgePrice();
            obj.Id =Convert.ToInt32(frm["hdnId"]);
            obj = db.DeliveryType_SurgePrice.Find(obj.Id);           
            if(obj!=null)
            {
                obj.Id = obj.Id;
                obj.SurgePercentage=Convert.ToInt32(frm["txtSurgePrice"]);
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("Index");

        }










        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.DeliveryType_SurgePrice.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsActive = false;
                obj.IsDeleted = true;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

    }
}