﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateSignUpController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/UpdateSignUp
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.Add_SignUp.SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");

        }



        [HttpPost]
        public ActionResult Update(FormCollection fm, HttpPostedFileBase[] files)
        {
            Add_SignUp obj = new Add_SignUp();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;
                    obj.BackGroundImage = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    obj.Id = Convert.ToInt32(fm["hdnId"]);
                    db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }




        [HttpPost]
        public ActionResult Add(FormCollection fm, HttpPostedFileBase[] files)
        {
            Add_SignUp obj = new Add_SignUp();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"~\img\" + pic;
                    obj.BackGroundImage = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    db.Add_SignUp.Add(obj);
                    db.SaveChanges();
                }
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}