﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateSurgeController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/UpdateSurge
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.Surges.Include(ab=>ab.Branch).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


        }

        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Surges.Where(c => c.Id.Equals(id)).SingleOrDefault();
            db.Surges.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.Surges.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            Surge obj = new Surge();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Surges.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.SurgeAmount = frm["SurgeAmount"];
                obj.IsActive = true;
                obj.Isdelete = false;
                obj.Createdate = DateTime.UtcNow;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


    }
}