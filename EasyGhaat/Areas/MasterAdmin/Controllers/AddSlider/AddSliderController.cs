﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddSliderController : Controller
    {

        AdminContext con = new AdminContext();

        public ActionResult Index()
        {
            return View(con.home_slider.ToList());
        }


        [HttpPost]
        public ActionResult AddSlider(HttpPostedFileBase[] files, home_slider slider)
        {
            //home_slider slider = new home_slider();
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images/" + pic;
                    slider.image_path = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    slider.content_1 = Request.Form["content1"];
                    slider.content_2 = Request.Form["content2"];
                    con.home_slider.Add(slider);
                    con.SaveChanges();
                }
            }

            TempData["Message"] = "Successfull";
            // return new          JavascriptResult() { Script = "alert('Successfully registered');" };
            return RedirectToAction("Index");
        }



        //For Edit
        public ActionResult Edit(int id)
        {
            return View(con.home_slider.Where(c => c.slider_id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit([Bind(Include = "slider_id,image_path,content_1,content_2")] home_slider slider)

        {
            con.Entry(slider).State = System.Data.Entity.EntityState.Modified;
            con.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        //For Delete
        public ActionResult Delete(int id)
        {
            var sl = con.home_slider.Where(c => c.slider_id.Equals(id)).SingleOrDefault();
            con.home_slider.Remove(sl);
            con.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }










    }
}