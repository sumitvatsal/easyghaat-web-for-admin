﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers.AddSlider
{
    public class UpdateSliderController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/UpdateSlider
        public ActionResult Index()
        {
            return View();
        }




       // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.home_slider.Where(c => c.slider_id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            home_slider obj = new home_slider();
            obj.slider_id= Convert.ToInt32(frm["hdnId"]);
            obj = db.home_slider.Find(obj.slider_id);
            if(obj!=null)
            { 
                obj.slider_id= Convert.ToInt32(frm["hdnId"]);
                obj.content_1 = frm["Title"];
                obj.content_2 = frm["SubTitle"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../AddSlider/Index");
        }








    }
}