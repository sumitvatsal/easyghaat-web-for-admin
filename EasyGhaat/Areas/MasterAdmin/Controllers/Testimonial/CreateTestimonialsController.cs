﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers.Testimonial
{
    public class CreateTestimonialsController : Controller
    {
        AdminContext con = new AdminContext();

        // GET: MasterAdmin/CreateTestimonials
        public ActionResult Index()
        {
            return View(con.testimonials.ToList());
        }

        [HttpPost]
        public ActionResult ADD(HttpPostedFileBase[] files, testimonial t1)
        {

            foreach (HttpPostedFileBase file in files)

                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string relativePath = @"/Content/images" + pic;

                    t1.image_path = relativePath;
                    //file.SaveAs(LocalPath);
                    file.SaveAs(Server.MapPath(relativePath));
                    t1.testi_content = Request.Form["message"];
                    con.testimonials.Add(t1);
                    con.SaveChanges();
                    
                }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        //For Delete
        public ActionResult Delete(int id)
        {
            var sl = con.testimonials.Where(c => c.tesit_id.Equals(id)).SingleOrDefault();
            con.testimonials.Remove(sl);
            con.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }












    }

    }
