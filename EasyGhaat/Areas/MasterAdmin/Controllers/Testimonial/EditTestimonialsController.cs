﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EasyGhaat.Areas.MasterAdmin.Controllers.Testimonial
{
    public class EditTestimonialsController : Controller
    {

        AdminContext db = new AdminContext();
        // GET: MasterAdmin/EditTestimonials
        public ActionResult Index()
        {           
            return View();
        }

        //For Edit

        public ActionResult Edit(int id)
        {
            return View(db.testimonials.Where(c => c.tesit_id.Equals(id)).SingleOrDefault());
        }

        //Edit Testimonials
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            testimonial obj = new testimonial();
            obj.tesit_id = Convert.ToInt32(frm["hdnId"]);
            obj = db.testimonials.Find(obj.tesit_id);
            if (obj != null)
            {
                obj.tesit_id = Convert.ToInt32(frm["hdnId"]);
                obj.testi_content = frm["Title"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../CreateTestimonials/Index");
        }

    }
}