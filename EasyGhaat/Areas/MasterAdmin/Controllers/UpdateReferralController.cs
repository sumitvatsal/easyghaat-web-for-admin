﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateReferralController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateReferral
        public ActionResult Index(int id)
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.ManageReferrals.Where(c => c.Id.Equals(id)).SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");

        }

        // For Edit
        //public ActionResult Edit(int id)
        //{
        //    return View(db.ManageReferrals.Where(c => c.Id.Equals(id)).SingleOrDefault());
        //}

        //For Submition of Edit



        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            ManageReferral obj = new ManageReferral();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.ManageReferrals.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);    
                obj.IsActive = false;
                obj.IsDeleted = false;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            obj.BranchId =Convert.ToInt32(frm["hdnIdbid"]);
            obj.ReferralAmount = frm["Amount"];
            obj.ValidityDays = frm["Validity"];
            obj.IsActive = true;
            obj.IsDeleted = false;
            obj.CreateDate = DateTime.UtcNow;
            db.ManageReferrals.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("../ViewReferrals/Index");
        }



    }
}