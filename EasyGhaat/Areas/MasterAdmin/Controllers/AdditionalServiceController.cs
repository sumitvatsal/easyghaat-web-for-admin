﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AdditionalServiceController : Controller
    {

        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AdditionalService
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Aditional_servicepage.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");




        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Submit(HttpPostedFileBase[] files)
        {
            Aditional_servicepage obj = new Aditional_servicepage();
            
                    obj.box_content = Request.Form["BoxContent"];
                    obj.box_heading = Request.Form["BoxHeading"];
                    db.Aditional_servicepage.Add(obj);
                    db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Aditional_servicepage.Where(c => c.id.Equals(id)).SingleOrDefault();
            db.Aditional_servicepage.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }


        public ActionResult Edit(int id)
        {
            return View(db.Aditional_servicepage.Where(c => c.id.Equals(id)).SingleOrDefault());
        }

        //Edit Testimonials
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            Aditional_servicepage obj = new Aditional_servicepage();
            obj.id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Aditional_servicepage.Find(obj.id);
            if (obj != null)
            {
                obj.id = Convert.ToInt32(frm["hdnId"]);
                obj.box_heading = frm["BoxHeading"];
                obj.box_content = frm["BoxContent"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../AdditionalService/Index");
        }


    }
}