﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateProductsController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateProducts
        public ActionResult Index(int id)
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Products.Where(c => c.Id.Equals(id)).SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");
           
        }



        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase[] files, FormCollection frm)
        {
            Product obj = new Product();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Products.Find(obj.Id);
            if (obj != null)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string relativePath = @"/Content/images" + pic;
                        obj.ImgUrl = relativePath;
                        //file.SaveAs(LocalPath);
                        file.SaveAs(Server.MapPath(relativePath));
                        obj.Id = Convert.ToInt32(frm["hdnId"]);
                        obj.Name = frm["Name"];
                        obj.Description = frm["Description"];
                        db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }

                    else
                    {

                        obj.Id = Convert.ToInt32(frm["hdnId"]);
                        obj.Name = frm["Name"];
                        obj.Description = frm["Description"];
                        db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }

                }

            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../ViewProducts/Index");
        }






    }
}