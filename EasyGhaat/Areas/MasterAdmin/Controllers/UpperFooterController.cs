﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpperFooterController : Controller
    {

        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpperFooter
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.UpperFooters.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");

        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Submit(UpperFooter obj, FormCollection frm)
        {

            obj.Heading = frm["Heading"];
            obj.Content= frm["Content3"];
            obj.Icon = frm["Icon"];
            obj.ContactNumber = frm["ContactNumber"];
            db.UpperFooters.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.UpperFooters.Where(c => c.Id.Equals(id)).SingleOrDefault();
            db.UpperFooters.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        //For Edit
        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(db.UpperFooters.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(FormCollection frm)
        {
            UpperFooter obj = new UpperFooter();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.UpperFooters.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);

                obj.Heading = frm["Heading"];
                obj.Content = frm["Content3"];
                obj.Icon = frm["Icon"];
                obj.ContactNumber = frm["ContactNumber"];
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

    }
}