﻿using EasyGhaat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageOrderController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ManageOrder
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {



                ViewModel vm = new ViewModel();
                vm.Branch = db.Branches.ToList();
                return View(vm);
            }

            return RedirectToAction("../../AdminLogin/Index");

        }
    }
}