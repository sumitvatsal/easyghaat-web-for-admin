﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewEmployeesController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewEmployees
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }

        [HttpPost]
        public JsonResult UpdateEmployee(Employee objemployee)
        {
            try
            {              
                var Result = db.Employees.Find(objemployee.emp_id);
                if (Result != null)
                {
                    Result.emp_id = objemployee.emp_id;
                    Result.emp_name = objemployee.emp_name;
                    Result.EmailId = objemployee.EmailId;
                    Result.per_phoneNo = objemployee.per_phoneNo;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
        [HttpPost]
       public JsonResult CheckEmail(Employee objemployee)
        {
            var CheckEmail = db.Employees.Where(emp => emp.EmailId == objemployee.EmailId).SingleOrDefault();
            if (CheckEmail != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CheckPhoneNo(Employee objemployee)
        {
            var CheckPhoneNo = db.Employees.Where(emp => emp.per_phoneNo == objemployee.per_phoneNo).SingleOrDefault();
            if (CheckPhoneNo != null)
            { 
                
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateSalary(SalaryDetails objSalaryDetail)
        {
            try
            {
                var Result = db.SalaryDetails.Where(sd => sd.EmpId == objSalaryDetail.EmpId && sd.IsDeleted == false).SingleOrDefault();
                if (Result != null)
                {
                    Result.EmpId = objSalaryDetail.EmpId;
                    Result.IsDeleted = true;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                objSalaryDetail.CreateDate = indianTime;
                objSalaryDetail.IsActive = true;
                db.SalaryDetails.Add(objSalaryDetail);
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
        [HttpPost]
        public JsonResult UpdateBankDetail(EmployeeBankAccount_Tbl objEmployeeBankAccount_Tbl)
        {
            try
            {
                var BankAccountNumber = objEmployeeBankAccount_Tbl.BankAccountNumber;
                var Check = db.EmployeeBankAccount_Tbl.Where(e => e.BankAccountNumber == BankAccountNumber && e.IsDeleted == false).SingleOrDefault();
                if (Check != null)
                {
                    Check.EmployeeId = objEmployeeBankAccount_Tbl.EmployeeId;
                    Check.BankAccountNumber = objEmployeeBankAccount_Tbl.BankAccountNumber;
                    Check.BankName = objEmployeeBankAccount_Tbl.BankName;
                    Check.BranchName = objEmployeeBankAccount_Tbl.BranchName;
                    Check.IFSC = objEmployeeBankAccount_Tbl.IFSC;
                    Check.MICR = objEmployeeBankAccount_Tbl.MICR;
                    Check.AccountType = objEmployeeBankAccount_Tbl.AccountType;
                    db.Entry(Check).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                var Result = db.EmployeeBankAccount_Tbl.Where(sd => sd.EmployeeId == objEmployeeBankAccount_Tbl.EmployeeId && sd.IsDeleted == false).SingleOrDefault();
                if (Result != null)
                    {
                        Result.EmployeeId = objEmployeeBankAccount_Tbl.EmployeeId;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objEmployeeBankAccount_Tbl.CreateDate = indianTime;
                    objEmployeeBankAccount_Tbl.IsActive = true;
                    db.EmployeeBankAccount_Tbl.Add(objEmployeeBankAccount_Tbl);
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
        [HttpPost]
        public JsonResult UpdateAddress(EmployeeAddress objEmployeeAddress)
        {
            try
            {
                if (objEmployeeAddress.AddTypeId == 4)
                {
                    var Result = db.EmployeeAddresses.Where(sd => sd.EmployeeId == objEmployeeAddress.EmployeeId && sd.IsDeleted == false && sd.AddTypeId == 4).SingleOrDefault();
                    if (Result != null)
                    {
                        Result.EmployeeId = objEmployeeAddress.EmployeeId;
                        Result.AddTypeId = 4;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objEmployeeAddress.CreateDate = indianTime;
                    objEmployeeAddress.IsActive = true;
                    db.EmployeeAddresses.Add(objEmployeeAddress);
                    db.SaveChanges();
                }
                if (objEmployeeAddress.AddTypeId == 5)
                {

                    var Result = db.EmployeeAddresses.Where(sd => sd.EmployeeId == objEmployeeAddress.EmployeeId && sd.IsDeleted == false && sd.AddTypeId == 5).SingleOrDefault();
                    if (Result != null)
                    {
                        Result.EmployeeId = objEmployeeAddress.EmployeeId;
                        Result.AddTypeId = 5;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objEmployeeAddress.CreateDate = indianTime;
                    objEmployeeAddress.IsActive = true;
                    db.EmployeeAddresses.Add(objEmployeeAddress);
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
    }
}