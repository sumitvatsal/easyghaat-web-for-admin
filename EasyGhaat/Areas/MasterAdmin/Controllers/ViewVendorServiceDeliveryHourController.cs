﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewVendorServiceDeliveryHourController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewVendorServiceDeliveryHour
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.VendorServiceDeliveryHour_tbl.Where(v => v.IsDeleted == false).Include(v=>v.DeliveryType_tbl).Include(v=>v.Vendor).Include(v=>v.Service).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


            
        }


        public ActionResult Delete(int id)
        {
            var obj = db.VendorServiceDeliveryHour_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj != null)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }



        // For Edit
        public ActionResult Edit(int id)
        {
            return View(db.VendorServiceDeliveryHour_tbl.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm)
        {
            VendorServiceDeliveryHour_tbl obj = new VendorServiceDeliveryHour_tbl();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.VendorServiceDeliveryHour_tbl.Find(obj.Id);
            if (obj != null)
            {
                obj.Id = Convert.ToInt32(frm["hdnId"]);
                obj.ServiceHours = Convert.ToInt32(frm["txtServiceHours"]);
                obj.MarginalHours = Convert.ToInt32(frm["txtMarginalHours"]);
                obj.SurgePercentage = Convert.ToInt32(frm["txtSurgePrice"]);
                obj.IsActive = true;
                obj.IsDeleted = false;
        
                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }












    }
}