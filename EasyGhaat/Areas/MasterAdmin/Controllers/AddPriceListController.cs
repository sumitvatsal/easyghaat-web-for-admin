﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddPriceListController : Controller
    {
        AdminContext db = new AdminContext();
        ViewModel vm = new ViewModel();

        // GET: MasterAdmin/AddPriceList
        public ActionResult Index()
        {
           using (AdminContext db = new AdminContext())
            {
                int SessionId = Convert.ToInt32(Session["AdminId"]);
                if (SessionId != 0)
                {
                    vm.Product = db.Products.Where(p => p.IsDeleted == false && p.IsActive==true).ToList();
                    vm.Service = db.Services.Where(s => s.IsDeleted == false && s.IsActive==true).ToList();
                    vm.Unit_Type = db.Unit_Type.Where(ut => ut.IsDeleted == false && ut.IsActive == true).ToList();
                    return View(vm);
                }
                return RedirectToAction("../../AdminLogin/Index");

            }
        }


           [HttpPost]
           public ActionResult Add(FormCollection frm)
                {
                    int pid = Convert.ToInt32(frm["ddlProduct"]);
                    int sid= Convert.ToInt32(frm["ddlService"]);
                    int bid=int.Parse(frm["ddlBranch"]);
                    PriceTable obj1 = new PriceTable();
                    Product obj = new Product();
                    var s = db.PriceTables.Where(i => i.ServiceId == sid && i.ProductId == pid &&  i.BranchId==bid && i.IsDeleted==false && i.IsActive==true).SingleOrDefault();
                    int si = Convert.ToInt32(s);
                    if( si!=0)
                    {
                     TempData["Message-1"] = "UnSuccessfull";
                     return RedirectToAction("Index");
                    }
                    else
                    {              
                    var p = db.Products.Find(pid);
                    obj1.ProductId = int.Parse(frm["ddlProduct"]);
                    obj1.BranchId = int.Parse(frm["ddlBranch"]);
                    obj1.Price = Convert.ToDouble(frm["Price"]);
                    obj1.ServiceId = sid;
                    obj1.UnitId = Convert.ToInt32(frm["ddlUnit"]);
                    obj1.IsDeleted = false;
                    obj1.IsActive = true;
                    db.PriceTables.Add(obj1);
                    db.SaveChanges();                  
                     }
                    return RedirectToAction("../ViewPriceList/Index");
                }

    }
}
    
