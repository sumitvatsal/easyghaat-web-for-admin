﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ManageBranchController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ManageBranch
        public ActionResult Index()
        {        
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {





                ViewModel mymodel = new ViewModel();
                mymodel.City = db.Cities.ToList();
                return View(mymodel);
            }

            return RedirectToAction("../../AdminLogin/Index");
                  
        }


        [HttpPost]
        public ActionResult Add(FormCollection frm)
        {
            Branch obj = new Branch();
            obj.Name = frm["BranchName"];
            obj.Address = frm["BranchAddress"];
            obj.CityId = int.Parse(frm["BranchCity"]);
            obj.PhoneNumber = frm["PhoneNumber"];
            obj.FaxNo = frm["FaxNumber"];
            obj.EmailId =frm["EmailId"];
            obj.IsActive = false;
            obj.IsDeleted = false;
            obj.CreatedDate = DateTime.UtcNow;
            db.Branches.Add(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");

        }

    }
}