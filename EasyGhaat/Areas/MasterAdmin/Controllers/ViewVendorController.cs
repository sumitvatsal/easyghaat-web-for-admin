﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewVendorController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/ViewVendor
        public ActionResult Index()
        {

            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");
        }

        [HttpPost]
        public JsonResult UpdateVendor(Vendor objVendor)
        {
            try
            {
                var Result = db.Vendors.Find(objVendor.Id);
                if (Result != null)
                {
                    Result.Id = objVendor.Id;
                    Result.Password = objVendor.Password;
                    Result.Per_PhoneNo = objVendor.Per_PhoneNo;
                    Result.Email = objVendor.Email;
                    Result.DOJ = objVendor.DOJ;
                    Result.DateOfContract = objVendor.DateOfContract;
                    Result.ServiceTaxNo = objVendor.ServiceTaxNo;
                    Result.PhoneNo = objVendor.PhoneNo;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }


        [HttpPost]
        public JsonResult CheckEmail(Vendor objVendor)
        {
            var CheckEmail = db.Vendors.Where(emp => emp.Email == objVendor.Email).SingleOrDefault();
            if (CheckEmail != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InActiveVendor(Vendor objVendor)
        {
            var Result = db.Vendors.Where(emp => emp.Id == objVendor.Id).SingleOrDefault();
            if (Result != null)
            {
                Result.Id = objVendor.Id;
                Result.IsActive = false;
                db.Entry(Result).State = EntityState.Modified;
                db.SaveChanges();        
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }





        [HttpPost]
        public JsonResult CheckPhoneNo(Vendor objVendor)
        {
            var CheckPhoneNo = db.Vendors.Where(emp => emp.Per_PhoneNo == objVendor.Per_PhoneNo).SingleOrDefault();
            if (CheckPhoneNo != null)
            {

                return Json(false, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateBankDetail(VendorBankAccount_Tbl objVendorBankAccount_Tbl)
        {
            try
            {
                var BankAccountNumber = objVendorBankAccount_Tbl.BankAccountNumber;
                var Check = db.VendorBankAccount_Tbl.Where(e => e.BankAccountNumber == BankAccountNumber && e.IsDeleted == false).SingleOrDefault();
                if (Check != null)
                {
                    Check.VendorId = objVendorBankAccount_Tbl.VendorId;
                    Check.BankAccountNumber = objVendorBankAccount_Tbl.BankAccountNumber;
                    Check.BankName = objVendorBankAccount_Tbl.BankName;
                    Check.BranchName = objVendorBankAccount_Tbl.BranchName;
                    Check.IFSC = objVendorBankAccount_Tbl.IFSC;
                    Check.MICR = objVendorBankAccount_Tbl.MICR;
                    Check.AccountType = objVendorBankAccount_Tbl.AccountType;
                    db.Entry(Check).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    var Result = db.VendorBankAccount_Tbl.Where(sd => sd.VendorId == objVendorBankAccount_Tbl.VendorId && sd.IsDeleted == false).SingleOrDefault();
                    if (Result != null)
                    {
                        Result.VendorId = objVendorBankAccount_Tbl.VendorId;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objVendorBankAccount_Tbl.CreateDate = indianTime;
                    objVendorBankAccount_Tbl.IsActive = true;
                    db.VendorBankAccount_Tbl.Add(objVendorBankAccount_Tbl);
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        [HttpPost]
        public JsonResult UpdateAddress(VendorAddress objVendorAddress)
        {
            try
            {
                if (objVendorAddress.AddTypeId == 4)
                {
                    var Result = db.VendorAddresses.Where(sd => sd.VendorId == objVendorAddress.VendorId && sd.IsDeleted == false && sd.AddTypeId == 4).SingleOrDefault();
                    if (Result != null)
                    {
                        Result.VendorId = objVendorAddress.VendorId;
                        Result.AddTypeId = 4;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objVendorAddress.CreateDate = indianTime;
                    objVendorAddress.IsActive = true;
                    objVendorAddress.IsDeleted = false;
                    db.VendorAddresses.Add(objVendorAddress);
                    db.SaveChanges();
                }
                if (objVendorAddress.AddTypeId == 5)
                {
                    var Result = db.VendorAddresses.Where(sd => sd.VendorId == objVendorAddress.VendorId && sd.IsDeleted == false && sd.AddTypeId == 5).SingleOrDefault();
                    if (Result != null)
                    {
                        Result.VendorId = objVendorAddress.VendorId;
                        Result.AddTypeId = 5;
                        Result.IsDeleted = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");//Getting Indian Time
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);//Getting Indian Time
                    objVendorAddress.CreateDate = indianTime;
                    objVendorAddress.IsActive = true;
                    objVendorAddress.IsDeleted = false;
                    db.VendorAddresses.Add(objVendorAddress);
                    db.SaveChanges();
                }
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting  
                        // the current instance as InnerException  
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

    }
}