﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Data.SqlClient;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class ViewReferralsController : Controller
    {
        AdminContext db = new AdminContext();
        // GET: MasterAdmin/ViewReferrals

        public ActionResult Index()
        {


            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                return View(db.ManageReferrals.Where(e => e.IsDeleted == false && e.IsActive == true).Include(ab=>ab.Branch).ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");
           
        }
    }
}