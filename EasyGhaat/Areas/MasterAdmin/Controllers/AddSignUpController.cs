﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;


namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class AddSignUpController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/AddSignUp
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {
                return View(db.Mail_collection.ToList());
            }

            return RedirectToAction("../../AdminLogin/Index");


        }




        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Mail_collection.Where(c => c.id.Equals(id)).SingleOrDefault();
            db.Mail_collection.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }







    }
}