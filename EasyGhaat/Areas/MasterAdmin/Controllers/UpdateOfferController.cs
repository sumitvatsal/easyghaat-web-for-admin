﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateOfferController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateOffer
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {


                IList<Offer> obj = db.Offers.ToList();
                return View(obj);
            }

            return RedirectToAction("../../AdminLogin/Index");



        }

      
        public ActionResult Edit(int id)
        {
            return View(db.Offers.Where(c => c.Id.Equals(id)).SingleOrDefault());
        }

        //For Submition of Edit
        [HttpPost]
        public ActionResult Edit(FormCollection frm )
        {
            Offer obj = new Offer();
            int OfferId = Convert.ToInt32(frm["hdnId"]);
            obj = db.Offers.Find(OfferId);
            if(obj!=null)
            { 
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj.OfferTitle = frm["OfferTitle"];
            obj.OfferContent = frm["OfferContent"];
            obj.OfferPrice = frm["OfferPrice"];
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            }
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

        //For Delete
        public ActionResult Delete(int id)
        {
            var obj = db.Offers.Where(c => c.Id.Equals(id)).SingleOrDefault();
            db.Offers.Remove(obj);
            db.SaveChanges();
            TempData["Message"] = "Successfull";
            return RedirectToAction("Index");
        }

    }
}