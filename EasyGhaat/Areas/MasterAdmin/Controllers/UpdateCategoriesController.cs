﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateCategoriesController : Controller
    {
        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateCategories
        public ActionResult Index(int id)
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View(db.Categories.Where(c => c.Id.Equals(id)).SingleOrDefault());
            }

            return RedirectToAction("../../AdminLogin/Index");

        }


        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase[] files, FormCollection frm)
        {
            Category obj = new Category();
            obj.Id = Convert.ToInt32(frm["hdnId"]);
            obj = db.Categories.Find(obj.Id);
            if (obj != null)
            {
             
                        obj.Id = Convert.ToInt32(frm["hdnId"]);
                        obj.Name = frm["Name"];
                        obj.Description = frm["Description"];
                        db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
               }
            TempData["Message"] = "Successfull";
            return RedirectToAction("../ViewCategory/Index");

        }
  
    }
}