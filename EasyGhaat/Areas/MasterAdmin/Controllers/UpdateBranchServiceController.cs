﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using EasyGhaat.Models;
using System.IO;
using System.Data;
using System.Web.UI.WebControls;

namespace EasyGhaat.Areas.MasterAdmin.Controllers
{
    public class UpdateBranchServiceController : Controller
    {

        AdminContext db = new AdminContext();

        // GET: MasterAdmin/UpdateBranchService
        public ActionResult Index()
        {
            int SessionId = Convert.ToInt32(Session["AdminId"]);

            if (SessionId != 0)
            {

                return View();
            }

            return RedirectToAction("../../AdminLogin/Index");

        }


        //[HttpGet]
        //public JsonResult GetService(string sidx, string sord, int page, int rows)
        //{
        //    using (AdminContext db = new AdminContext())
        //    {
        //        int pageIndex = Convert.ToInt32(page) - 1;
        //        int pageSize = rows;
        //        var obj = db.Database.SqlQuery<Proc_GetService_Result>("Proc_GetService").ToList<Proc_GetService_Result>();

        //        int totalRecords = obj.Count();
        //        var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
        //        if (sord.ToUpper() == "DESC")
        //        {
        //            obj = obj.OrderByDescending(s => s.Id).ToList();
        //            obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //        else
        //        {
        //            obj = obj.OrderBy(s => s.Id).ToList();
        //            obj = obj.Skip(pageIndex * pageSize).Take(pageSize).ToList();
        //        }
        //        var jsonData = new
        //        {
        //            total = totalPages,
        //            page,
        //            records = totalRecords,
        //            rows = obj
        //        };
        //        return Json(jsonData, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //Edit Product
        public string Edit(Service obj)
        {
            int Id = obj.Id;
            var Newobj = db.Services.Find(Id);
            string msg = "";

            if (Newobj != null)
            {
                Newobj.Id = obj.Id;
                Newobj.Name = obj.Name;
                Newobj.Description = obj.Description;
                Newobj.IsActive = obj.IsActive;
                db.Entry(Newobj).State = EntityState.Modified;
                db.SaveChanges();
            }
            return msg = "Saved Successfully";
        }

        //For Delete
        public string Delete(int id)
        {
            var obj = db.Services.Where(c => c.Id.Equals(id)).SingleOrDefault();
            if (obj.IsDeleted == false)
            {
                obj.Id = id;
                obj.IsDeleted = true;
                obj.IsActive = false;
                obj.CreateDate = DateTime.UtcNow;
                db.SaveChanges();
            }
            return "Deleted successfully";
        }









    }
}